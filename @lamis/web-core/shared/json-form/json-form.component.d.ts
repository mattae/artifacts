import { EventEmitter, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { LayoutTemplateService } from '../../services/layout.template.service';
import { FormioComponent } from 'angular-material-formio';
export declare class JsonFormComponent implements OnInit, OnChanges {
    private templateService;
    formio: FormioComponent;
    template: string;
    model: any;
    dataEvent: EventEmitter<any>;
    form: any;
    isValid: boolean;
    private _defaultValue;
    constructor(templateService: LayoutTemplateService);
    ngOnInit(): void;
    reset(): void;
    change(event: any): void;
    ngOnChanges(changes: SimpleChanges): void;
}
