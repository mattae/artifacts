import * as tslib_1 from "tslib";
import { Inject, Injectable, Injector } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { SERVER_API_URL_CONFIG } from '../../app.constants';
let AuthInterceptor = class AuthInterceptor {
    constructor(injector, serverUrl) {
        this.injector = injector;
        this.serverUrl = serverUrl;
        this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
    }
    intercept(request, next) {
        if (!request || !request.url || (/^http/.test(request.url) && !(this.serverUrl.SERVER_API_URL && request.url.startsWith(this.serverUrl.SERVER_API_URL)))) {
            return next.handle(request);
        }
        const token = this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
        if (!!token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token
                }
            });
        }
        return next.handle(request);
    }
};
AuthInterceptor.ctorParameters = () => [
    { type: Injector },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
AuthInterceptor = tslib_1.__decorate([
    Injectable(),
    tslib_1.__param(1, Inject(SERVER_API_URL_CONFIG)),
    tslib_1.__metadata("design:paramtypes", [Injector, Object])
], AuthInterceptor);
export { AuthInterceptor };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImJsb2Nrcy9pbnRlcmNlcHRvci9hdXRoLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFN0QsT0FBTyxFQUFFLG1CQUFtQixFQUFFLHFCQUFxQixFQUFFLE1BQU0sV0FBVyxDQUFDO0FBRXZFLE9BQU8sRUFBRSxxQkFBcUIsRUFBc0IsTUFBTSxxQkFBcUIsQ0FBQztBQUloRixJQUFhLGVBQWUsR0FBNUIsTUFBYSxlQUFlO0lBSTNCLFlBQW9CLFFBQWtCLEVBQXlDLFNBQTZCO1FBQXhGLGFBQVEsR0FBUixRQUFRLENBQVU7UUFBeUMsY0FBUyxHQUFULFNBQVMsQ0FBb0I7UUFDM0csSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsU0FBUyxDQUFDLE9BQXlCLEVBQUUsSUFBaUI7UUFFckQsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDekosT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzVCO1FBRUQsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQy9HLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRTtZQUNaLE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUN2QixVQUFVLEVBQUU7b0JBQ1gsYUFBYSxFQUFFLFNBQVMsR0FBRyxLQUFLO2lCQUNoQzthQUNELENBQUMsQ0FBQztTQUNIO1FBQ0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzdCLENBQUM7Q0FDRCxDQUFBOztZQXJCOEIsUUFBUTs0Q0FBRyxNQUFNLFNBQUMscUJBQXFCOztBQUp6RCxlQUFlO0lBRDNCLFVBQVUsRUFBRTtJQUs2QixtQkFBQSxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQTs2Q0FBeEMsUUFBUTtHQUoxQixlQUFlLENBeUIzQjtTQXpCWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IExvY2FsU3RvcmFnZVNlcnZpY2UsIFNlc3Npb25TdG9yYWdlU2VydmljZSB9IGZyb20gJ25neC1zdG9yZSc7XHJcbmltcG9ydCB7IEh0dHBFdmVudCwgSHR0cEhhbmRsZXIsIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlcXVlc3QgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IFNFUlZFUl9BUElfVVJMX0NPTkZJRywgU2VydmVyQXBpVXJsQ29uZmlnIH0gZnJvbSAnLi4vLi4vYXBwLmNvbnN0YW50cyc7XHJcblxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXV0aEludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcclxuXHRwcml2YXRlICRsb2NhbFN0b3JhZ2U6IExvY2FsU3RvcmFnZVNlcnZpY2U7XHJcblx0cHJpdmF0ZSAkc2Vzc2lvblN0b3JhZ2U6IFNlc3Npb25TdG9yYWdlU2VydmljZTtcclxuXHJcblx0Y29uc3RydWN0b3IocHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IsIEBJbmplY3QoU0VSVkVSX0FQSV9VUkxfQ09ORklHKSBwcml2YXRlIHNlcnZlclVybDogU2VydmVyQXBpVXJsQ29uZmlnKSB7XHJcblx0XHR0aGlzLiRsb2NhbFN0b3JhZ2UgPSB0aGlzLmluamVjdG9yLmdldChMb2NhbFN0b3JhZ2VTZXJ2aWNlKTtcclxuXHRcdHRoaXMuJHNlc3Npb25TdG9yYWdlID0gdGhpcy5pbmplY3Rvci5nZXQoU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlKTtcclxuXHR9XHJcblxyXG5cdGludGVyY2VwdChyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcclxuXHJcblx0XHRpZiAoIXJlcXVlc3QgfHwgIXJlcXVlc3QudXJsIHx8ICgvXmh0dHAvLnRlc3QocmVxdWVzdC51cmwpICYmICEodGhpcy5zZXJ2ZXJVcmwuU0VSVkVSX0FQSV9VUkwgJiYgcmVxdWVzdC51cmwuc3RhcnRzV2l0aCh0aGlzLnNlcnZlclVybC5TRVJWRVJfQVBJX1VSTCkpKSkge1xyXG5cdFx0XHRyZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XHJcblx0XHR9XHJcblxyXG5cdFx0Y29uc3QgdG9rZW4gPSB0aGlzLiRsb2NhbFN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJykgfHwgdGhpcy4kc2Vzc2lvblN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJyk7XHJcblx0XHRpZiAoISF0b2tlbikge1xyXG5cdFx0XHRyZXF1ZXN0ID0gcmVxdWVzdC5jbG9uZSh7XHJcblx0XHRcdFx0c2V0SGVhZGVyczoge1xyXG5cdFx0XHRcdFx0QXV0aG9yaXphdGlvbjogJ0JlYXJlciAnICsgdG9rZW5cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xyXG5cdH1cclxufVxyXG4iXX0=