import * as tslib_1 from "tslib";
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { LayoutTemplateService } from '../../services/layout.template.service';
import { FormioComponent } from 'angular-material-formio';
let JsonFormComponent = class JsonFormComponent {
    constructor(templateService) {
        this.templateService = templateService;
        this.dataEvent = new EventEmitter(true);
        this.form = {};
        this.isValid = false;
    }
    ngOnInit() {
        this.templateService.getTemplate(this.template).subscribe((json) => {
            this.form = json.template;
        });
    }
    reset() {
        this.model = this._defaultValue;
    }
    change(event) {
        this.isValid = event.isValid;
        if (this.isValid) {
            this.dataEvent.emit(event.data);
        }
    }
    ngOnChanges(changes) {
        if (changes['model']) {
            this.model = changes['model'];
            this._defaultValue = changes['model'];
            this.formio.submission = this.model;
        }
    }
};
JsonFormComponent.ctorParameters = () => [
    { type: LayoutTemplateService }
];
tslib_1.__decorate([
    ViewChild(FormioComponent, { static: true }),
    tslib_1.__metadata("design:type", FormioComponent)
], JsonFormComponent.prototype, "formio", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], JsonFormComponent.prototype, "template", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], JsonFormComponent.prototype, "model", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], JsonFormComponent.prototype, "dataEvent", void 0);
JsonFormComponent = tslib_1.__decorate([
    Component({
        selector: 'json-form',
        template: `
        <mat-formio [form]="form"
                    [submission]="model"
                    [options]=""
                    (change)="change($event)">
        </mat-formio>
    `
    }),
    tslib_1.__metadata("design:paramtypes", [LayoutTemplateService])
], JsonFormComponent);
export { JsonFormComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9qc29uLWZvcm0vanNvbi1mb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQWlCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUMvRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFZMUQsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFhMUIsWUFBb0IsZUFBc0M7UUFBdEMsb0JBQWUsR0FBZixlQUFlLENBQXVCO1FBTDFELGNBQVMsR0FBc0IsSUFBSSxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEQsU0FBSSxHQUFRLEVBQUUsQ0FBQztRQUNmLFlBQU8sR0FBRyxLQUFLLENBQUM7SUFJaEIsQ0FBQztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7WUFDcEUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELEtBQUs7UUFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDcEMsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFVO1FBQ2IsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQzdCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNuQztJQUNMLENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDOUIsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUM7WUFDakIsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUN2QztJQUNMLENBQUM7Q0FDSixDQUFBOztZQTNCd0MscUJBQXFCOztBQVgxRDtJQURDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUM7c0NBQ25DLGVBQWU7aURBQUM7QUFFeEI7SUFEQyxLQUFLLEVBQUU7O21EQUNTO0FBRWpCO0lBREMsS0FBSyxFQUFFOztnREFDRztBQUVYO0lBREMsTUFBTSxFQUFFO3NDQUNFLFlBQVk7b0RBQStCO0FBUjdDLGlCQUFpQjtJQVY3QixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsV0FBVztRQUNyQixRQUFRLEVBQUU7Ozs7OztLQU1UO0tBQ0osQ0FBQzs2Q0FjdUMscUJBQXFCO0dBYmpELGlCQUFpQixDQXdDN0I7U0F4Q1ksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkNoYW5nZXMsIE9uSW5pdCwgT3V0cHV0LCBTaW1wbGVDaGFuZ2VzLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTGF5b3V0VGVtcGxhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbGF5b3V0LnRlbXBsYXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtaW9Db21wb25lbnQgfSBmcm9tICdhbmd1bGFyLW1hdGVyaWFsLWZvcm1pbyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnanNvbi1mb3JtJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPG1hdC1mb3JtaW8gW2Zvcm1dPVwiZm9ybVwiXHJcbiAgICAgICAgICAgICAgICAgICAgW3N1Ym1pc3Npb25dPVwibW9kZWxcIlxyXG4gICAgICAgICAgICAgICAgICAgIFtvcHRpb25zXT1cIlwiXHJcbiAgICAgICAgICAgICAgICAgICAgKGNoYW5nZSk9XCJjaGFuZ2UoJGV2ZW50KVwiPlxyXG4gICAgICAgIDwvbWF0LWZvcm1pbz5cclxuICAgIGBcclxufSlcclxuZXhwb3J0IGNsYXNzIEpzb25Gb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG4gICAgQFZpZXdDaGlsZChGb3JtaW9Db21wb25lbnQsIHtzdGF0aWM6IHRydWV9KVxyXG4gICAgZm9ybWlvOiBGb3JtaW9Db21wb25lbnQ7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGVtcGxhdGU6IHN0cmluZztcclxuICAgIEBJbnB1dCgpXHJcbiAgICBtb2RlbDogYW55O1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBkYXRhRXZlbnQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcih0cnVlKTtcclxuICAgIGZvcm06IGFueSA9IHt9O1xyXG4gICAgaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgcHJpdmF0ZSBfZGVmYXVsdFZhbHVlOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSB0ZW1wbGF0ZVNlcnZpY2U6IExheW91dFRlbXBsYXRlU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMudGVtcGxhdGVTZXJ2aWNlLmdldFRlbXBsYXRlKHRoaXMudGVtcGxhdGUpLnN1YnNjcmliZSgoanNvbjogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybSA9IGpzb24udGVtcGxhdGU7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKSB7XHJcbiAgICAgICAgdGhpcy5tb2RlbCA9IHRoaXMuX2RlZmF1bHRWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2UoZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIHRoaXMuaXNWYWxpZCA9IGV2ZW50LmlzVmFsaWQ7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNWYWxpZCkge1xyXG4gICAgICAgICAgICB0aGlzLmRhdGFFdmVudC5lbWl0KGV2ZW50LmRhdGEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGNoYW5nZXNbJ21vZGVsJ10pe1xyXG4gICAgICAgICAgICB0aGlzLm1vZGVsID0gY2hhbmdlc1snbW9kZWwnXTtcclxuICAgICAgICAgICAgdGhpcy5fZGVmYXVsdFZhbHVlID0gY2hhbmdlc1snbW9kZWwnXTtcclxuICAgICAgICAgICAgdGhpcy5mb3JtaW8uc3VibWlzc2lvbiA9IHRoaXMubW9kZWw7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==