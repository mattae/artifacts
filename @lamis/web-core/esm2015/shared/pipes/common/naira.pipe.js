import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
import { CurrencyPipe } from "@angular/common";
let NairaPipe = class NairaPipe {
    constructor() {
        this.pipe = new CurrencyPipe('en');
    }
    transform(value, ...args) {
        return this.pipe.transform(value, '₦');
    }
};
NairaPipe = tslib_1.__decorate([
    Pipe({
        name: 'naira'
    })
], NairaPipe);
export { NairaPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmFpcmEucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9waXBlcy9jb21tb24vbmFpcmEucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBSy9DLElBQWEsU0FBUyxHQUF0QixNQUFhLFNBQVM7SUFIdEI7UUFJWSxTQUFJLEdBQUcsSUFBSSxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7SUFLMUMsQ0FBQztJQUhHLFNBQVMsQ0FBQyxLQUFVLEVBQUUsR0FBRyxJQUFXO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzNDLENBQUM7Q0FDSixDQUFBO0FBTlksU0FBUztJQUhyQixJQUFJLENBQUM7UUFDRixJQUFJLEVBQUUsT0FBTztLQUNoQixDQUFDO0dBQ1csU0FBUyxDQU1yQjtTQU5ZLFNBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQ3VycmVuY3lQaXBlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5cclxuQFBpcGUoe1xyXG4gICAgbmFtZTogJ25haXJhJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmFpcmFQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcbiAgICBwcml2YXRlIHBpcGUgPSBuZXcgQ3VycmVuY3lQaXBlKCdlbicpO1xyXG5cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogYW55LCAuLi5hcmdzOiBhbnlbXSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnBpcGUudHJhbnNmb3JtKHZhbHVlLCAn4oKmJyk7XHJcbiAgICB9XHJcbn1cclxuIl19