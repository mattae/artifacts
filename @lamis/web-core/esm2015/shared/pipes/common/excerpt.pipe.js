import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
let ExcerptPipe = class ExcerptPipe {
    transform(text, limit = 5) {
        if (text.length <= limit)
            return text;
        return text.substring(0, limit) + '...';
    }
};
ExcerptPipe = tslib_1.__decorate([
    Pipe({ name: 'excerpt' })
], ExcerptPipe);
export { ExcerptPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhjZXJwdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3BpcGVzL2NvbW1vbi9leGNlcnB0LnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBR3BELElBQWEsV0FBVyxHQUF4QixNQUFhLFdBQVc7SUFDcEIsU0FBUyxDQUFDLElBQVksRUFBRSxRQUFnQixDQUFDO1FBQ3JDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLO1lBQ3BCLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDO0lBQzVDLENBQUM7Q0FDSixDQUFBO0FBTlksV0FBVztJQUR2QixJQUFJLENBQUMsRUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFDLENBQUM7R0FDWCxXQUFXLENBTXZCO1NBTlksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuQFBpcGUoe25hbWU6ICdleGNlcnB0J30pXHJcbmV4cG9ydCBjbGFzcyBFeGNlcnB0UGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG4gICAgdHJhbnNmb3JtKHRleHQ6IHN0cmluZywgbGltaXQ6IG51bWJlciA9IDUpIHtcclxuICAgICAgICBpZiAodGV4dC5sZW5ndGggPD0gbGltaXQpXHJcbiAgICAgICAgICAgIHJldHVybiB0ZXh0O1xyXG4gICAgICAgIHJldHVybiB0ZXh0LnN1YnN0cmluZygwLCBsaW1pdCkgKyAnLi4uJztcclxuICAgIH1cclxufVxyXG4iXX0=