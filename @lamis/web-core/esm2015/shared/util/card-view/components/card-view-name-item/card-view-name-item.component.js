import * as tslib_1 from "tslib";
import { Component, Input, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewNameItemModel } from '../../models/card-view-name-item.model';
import { PersonName } from '../../../../model/address.model';
let CardViewNameItemComponent = class CardViewNameItemComponent {
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    ngOnChanges() {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
    }
    showProperty() {
        return this.displayEmpty || !this.property.isEmpty();
    }
    isEditable() {
        return this.editable && this.property.editable;
    }
    isClickable() {
        return this.property.clickable;
    }
    hasIcon() {
        return !!this.property.icon;
    }
    hasErrors() {
        return this.errorMessages && this.errorMessages.length;
    }
    setEditMode(editStatus) {
        this.inEdit = editStatus;
        setTimeout(() => {
            if (this.titleInput) {
                this.titleInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.firstNameInput) {
                this.firstNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.middleNameInput) {
                this.middleNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.surnameInput) {
                this.surnameInput.nativeElement.click();
            }
        }, 0);
    }
    reset() {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
        this.setEditMode(false);
    }
    update() {
        console.log('Property', this.property);
        if (this.property.isValid(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname))) {
            this.cardViewUpdateService.update(this.property, new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
            this.property.value = new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
        }
    }
    get displayValue() {
        return this.property.displayValue;
    }
    clicked() {
        this.cardViewUpdateService.clicked(this.property);
    }
};
CardViewNameItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", CardViewNameItemModel)
], CardViewNameItemComponent.prototype, "property", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], CardViewNameItemComponent.prototype, "editable", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], CardViewNameItemComponent.prototype, "displayEmpty", void 0);
tslib_1.__decorate([
    ViewChild('titleInput', { static: true }),
    tslib_1.__metadata("design:type", Object)
], CardViewNameItemComponent.prototype, "titleInput", void 0);
tslib_1.__decorate([
    ViewChild('firstNameInput', { static: true }),
    tslib_1.__metadata("design:type", Object)
], CardViewNameItemComponent.prototype, "firstNameInput", void 0);
tslib_1.__decorate([
    ViewChild('middleNameInput', { static: true }),
    tslib_1.__metadata("design:type", Object)
], CardViewNameItemComponent.prototype, "middleNameInput", void 0);
tslib_1.__decorate([
    ViewChild('titleInput', { static: true }),
    tslib_1.__metadata("design:type", Object)
], CardViewNameItemComponent.prototype, "surnameInput", void 0);
CardViewNameItemComponent = tslib_1.__decorate([
    Component({
        selector: 'card-view-name-item',
        template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #titleInput\r\n                               matInput\r\n                               [placeholder]=\"'Title'\"\r\n                               [(ngModel)]=\"editedTitle\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #firstNameInput\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'First name'\"\r\n                               [(ngModel)]=\"editedFirstName\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #middleNameInput\r\n                               matInput\r\n                               [placeholder]=\"'Middle name'\"\r\n                               [(ngModel)]=\"editedMiddleName\"\r\n                               [attr.data-automation-id]=\"'card-textitem-middlenameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #surnameInput\r\n                               matInput\r\n                               [placeholder]=\"'Surname'\"\r\n                               [(ngModel)]=\"editedSurname\"\r\n                               [attr.data-automation-id]=\"'card-textitem-surnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
    }),
    tslib_1.__metadata("design:paramtypes", [CardViewUpdateService])
], CardViewNameItemComponent);
export { CardViewNameItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LW5hbWUtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvY29tcG9uZW50cy9jYXJkLXZpZXctbmFtZS1pdGVtL2NhcmQtdmlldy1uYW1lLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDM0QsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDL0UsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBTTdELElBQWEseUJBQXlCLEdBQXRDLE1BQWEseUJBQXlCO0lBNkJsQyxZQUFvQixxQkFBNEM7UUFBNUMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQXhCaEUsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUcxQixpQkFBWSxHQUFZLElBQUksQ0FBQztRQWM3QixXQUFNLEdBQVksS0FBSyxDQUFDO0lBUXhCLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDN0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFDckQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztJQUNyRCxDQUFDO0lBRUQsWUFBWTtRQUNSLE9BQU8sSUFBSSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDekQsQ0FBQztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQztJQUVELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO0lBQ25DLENBQUM7SUFFRCxPQUFPO1FBQ0gsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7SUFDaEMsQ0FBQztJQUVELFNBQVM7UUFDTCxPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7SUFDM0QsQ0FBQztJQUVELFdBQVcsQ0FBQyxVQUFtQjtRQUMzQixJQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztRQUN6QixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ1osSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUN6QztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNOLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDWixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQzdDO1FBQ0wsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ04sVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDdEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDOUM7UUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDTixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ1osSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUMzQztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDN0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFDckQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFRCxNQUFNO1FBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRTtZQUMxSCxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQzNDLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDdkcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDeEgsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7U0FDN0o7SUFDTCxDQUFDO0lBRUQsSUFBSSxZQUFZO1FBQ1osT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQztJQUN0QyxDQUFDO0lBRUQsT0FBTztRQUNILElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7Q0FDSixDQUFBOztZQWpGOEMscUJBQXFCOztBQTNCaEU7SUFEQyxLQUFLLEVBQUU7c0NBQ0UscUJBQXFCOzJEQUFDO0FBR2hDO0lBREMsS0FBSyxFQUFFOzsyREFDa0I7QUFHMUI7SUFEQyxLQUFLLEVBQUU7OytEQUNxQjtBQUc3QjtJQURDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUM7OzZEQUNoQjtBQUd4QjtJQURDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQzs7aUVBQ2hCO0FBRzVCO0lBREMsU0FBUyxDQUFDLGlCQUFpQixFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDOztrRUFDaEI7QUFHN0I7SUFEQyxTQUFTLENBQUMsWUFBWSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDOzsrREFDZDtBQXBCakIseUJBQXlCO0lBSnJDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxxQkFBcUI7UUFDL0Isb3BMQUFtRDtLQUN0RCxDQUFDOzZDQThCNkMscUJBQXFCO0dBN0J2RCx5QkFBeUIsQ0E4R3JDO1NBOUdZLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcywgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3VXBkYXRlU2VydmljZSB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3TmFtZUl0ZW1Nb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9jYXJkLXZpZXctbmFtZS1pdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgUGVyc29uTmFtZSB9IGZyb20gJy4uLy4uLy4uLy4uL21vZGVsL2FkZHJlc3MubW9kZWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NhcmQtdmlldy1uYW1lLWl0ZW0nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NhcmQtdmlldy1uYW1lLWl0ZW0uY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld05hbWVJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuICAgIEBJbnB1dCgpXHJcbiAgICBwcm9wZXJ0eTogQ2FyZFZpZXdOYW1lSXRlbU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBlZGl0YWJsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkaXNwbGF5RW1wdHk6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ3RpdGxlSW5wdXQnLCB7c3RhdGljOiB0cnVlfSlcclxuICAgIHByaXZhdGUgdGl0bGVJbnB1dDogYW55O1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ2ZpcnN0TmFtZUlucHV0Jywge3N0YXRpYzogdHJ1ZX0pXHJcbiAgICBwcml2YXRlIGZpcnN0TmFtZUlucHV0OiBhbnk7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgnbWlkZGxlTmFtZUlucHV0Jywge3N0YXRpYzogdHJ1ZX0pXHJcbiAgICBwcml2YXRlIG1pZGRsZU5hbWVJbnB1dDogYW55O1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ3RpdGxlSW5wdXQnLCB7c3RhdGljOiB0cnVlfSlcclxuICAgIHByaXZhdGUgc3VybmFtZUlucHV0OiBhbnk7XHJcblxyXG4gICAgaW5FZGl0OiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBlZGl0ZWRUaXRsZTogc3RyaW5nO1xyXG4gICAgZWRpdGVkU3VybmFtZTogc3RyaW5nO1xyXG4gICAgZWRpdGVkRmlyc3ROYW1lOiBzdHJpbmc7XHJcbiAgICBlZGl0ZWRNaWRkbGVOYW1lOiBzdHJpbmc7XHJcbiAgICBlcnJvck1lc3NhZ2VzOiBzdHJpbmdbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRWaWV3VXBkYXRlU2VydmljZTogQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRUaXRsZSA9IHRoaXMucHJvcGVydHkudmFsdWUudGl0bGU7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRGaXJzdE5hbWUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLmZpcnN0TmFtZTtcclxuICAgICAgICB0aGlzLmVkaXRlZE1pZGRsZU5hbWUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLm1pZGRsZU5hbWU7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRTdXJuYW1lID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5zdXJuYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dQcm9wZXJ0eSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kaXNwbGF5RW1wdHkgfHwgIXRoaXMucHJvcGVydHkuaXNFbXB0eSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGlzRWRpdGFibGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWRpdGFibGUgJiYgdGhpcy5wcm9wZXJ0eS5lZGl0YWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBpc0NsaWNrYWJsZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wZXJ0eS5jbGlja2FibGU7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzSWNvbigpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gISF0aGlzLnByb3BlcnR5Lmljb247XHJcbiAgICB9XHJcblxyXG4gICAgaGFzRXJyb3JzKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZXJyb3JNZXNzYWdlcyAmJiB0aGlzLmVycm9yTWVzc2FnZXMubGVuZ3RoO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEVkaXRNb2RlKGVkaXRTdGF0dXM6IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmluRWRpdCA9IGVkaXRTdGF0dXM7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRpdGxlSW5wdXQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGl0bGVJbnB1dC5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAwKTtcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZmlyc3ROYW1lSW5wdXQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZmlyc3ROYW1lSW5wdXQubmF0aXZlRWxlbWVudC5jbGljaygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMCk7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLm1pZGRsZU5hbWVJbnB1dCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5taWRkbGVOYW1lSW5wdXQubmF0aXZlRWxlbWVudC5jbGljaygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMCk7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN1cm5hbWVJbnB1dCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdXJuYW1lSW5wdXQubmF0aXZlRWxlbWVudC5jbGljaygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRUaXRsZSA9IHRoaXMucHJvcGVydHkudmFsdWUudGl0bGU7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRGaXJzdE5hbWUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLmZpcnN0TmFtZTtcclxuICAgICAgICB0aGlzLmVkaXRlZE1pZGRsZU5hbWUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLm1pZGRsZU5hbWU7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRTdXJuYW1lID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5zdXJuYW1lO1xyXG4gICAgICAgIHRoaXMuc2V0RWRpdE1vZGUoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpOiB2b2lkIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnUHJvcGVydHknLCB0aGlzLnByb3BlcnR5KTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wZXJ0eS5pc1ZhbGlkKG5ldyBQZXJzb25OYW1lKHRoaXMuZWRpdGVkVGl0bGUsIHRoaXMuZWRpdGVkRmlyc3ROYW1lLCB0aGlzLmVkaXRlZE1pZGRsZU5hbWUsIHRoaXMuZWRpdGVkU3VybmFtZSkpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2FyZFZpZXdVcGRhdGVTZXJ2aWNlLnVwZGF0ZSh0aGlzLnByb3BlcnR5LFxyXG4gICAgICAgICAgICAgICAgbmV3IFBlcnNvbk5hbWUodGhpcy5lZGl0ZWRUaXRsZSwgdGhpcy5lZGl0ZWRGaXJzdE5hbWUsIHRoaXMuZWRpdGVkTWlkZGxlTmFtZSwgdGhpcy5lZGl0ZWRTdXJuYW1lKSk7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcGVydHkudmFsdWUgPSBuZXcgUGVyc29uTmFtZSh0aGlzLmVkaXRlZFRpdGxlLCB0aGlzLmVkaXRlZEZpcnN0TmFtZSwgdGhpcy5lZGl0ZWRNaWRkbGVOYW1lLCB0aGlzLmVkaXRlZFN1cm5hbWUpO1xyXG4gICAgICAgICAgICB0aGlzLnNldEVkaXRNb2RlKGZhbHNlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmVycm9yTWVzc2FnZXMgPSB0aGlzLnByb3BlcnR5LmdldFZhbGlkYXRpb25FcnJvcnMobmV3IFBlcnNvbk5hbWUodGhpcy5lZGl0ZWRUaXRsZSwgdGhpcy5lZGl0ZWRGaXJzdE5hbWUsIHRoaXMuZWRpdGVkTWlkZGxlTmFtZSwgdGhpcy5lZGl0ZWRTdXJuYW1lKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBkaXNwbGF5VmFsdWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucHJvcGVydHkuZGlzcGxheVZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGNsaWNrZWQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jYXJkVmlld1VwZGF0ZVNlcnZpY2UuY2xpY2tlZCh0aGlzLnByb3BlcnR5KTtcclxuICAgIH1cclxufVxyXG4iXX0=