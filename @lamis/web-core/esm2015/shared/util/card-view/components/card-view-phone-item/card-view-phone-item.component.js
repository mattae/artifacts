import * as tslib_1 from "tslib";
import { Component, Input, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewPhoneItemModel } from '../../models/card-view-phone-item.model';
import { Phone } from '../../../../model/address.model';
let CardViewPhoneItemComponent = class CardViewPhoneItemComponent {
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    ngOnChanges() {
        this.editedPhone1 = this.property.value.phone1;
        this.editedPhone2 = this.property.value.phone2;
    }
    ngOnInit() {
    }
    showProperty() {
        return this.displayEmpty || !this.property.isEmpty();
    }
    isEditable() {
        return this.editable && this.property.editable;
    }
    isClickable() {
        return this.property.clickable;
    }
    hasIcon() {
        return !!this.property.icon;
    }
    hasErrors() {
        return this.errorMessages && this.errorMessages.length;
    }
    setEditMode(editStatus) {
        this.inEdit = editStatus;
        setTimeout(() => {
            if (this.phone1Input) {
                this.phone1Input.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.phone2Input) {
                this.phone2Input.nativeElement.click();
            }
        }, 0);
    }
    reset() {
        this.editedPhone1 = this.property.value.phone1;
        this.editedPhone2 = this.property.value.phone2;
        this.setEditMode(false);
    }
    update() {
        if (this.property.isValid(new Phone(this.editedPhone1, this.editedPhone2))) {
            this.cardViewUpdateService.update(this.property, new Phone(this.editedPhone1, this.editedPhone2));
            this.property.value = new Phone(this.editedPhone1, this.editedPhone2);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new Phone(this.editedPhone1, this.editedPhone2));
        }
    }
    get displayValue() {
        return this.property.displayValue;
    }
    clicked() {
        this.cardViewUpdateService.clicked(this.property);
    }
};
CardViewPhoneItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", CardViewPhoneItemModel)
], CardViewPhoneItemComponent.prototype, "property", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], CardViewPhoneItemComponent.prototype, "editable", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], CardViewPhoneItemComponent.prototype, "displayEmpty", void 0);
tslib_1.__decorate([
    ViewChild('phone1Input', { static: true }),
    tslib_1.__metadata("design:type", Object)
], CardViewPhoneItemComponent.prototype, "phone1Input", void 0);
tslib_1.__decorate([
    ViewChild('phone2Input', { static: true }),
    tslib_1.__metadata("design:type", Object)
], CardViewPhoneItemComponent.prototype, "phone2Input", void 0);
CardViewPhoneItemComponent = tslib_1.__decorate([
    Component({
        selector: 'card-view-phone-item',
        template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #phonr1Input\r\n                               matInput\r\n                               [placeholder]=\"'Phone 1'\"\r\n                               [(ngModel)]=\"editedPhone1\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #phone2Input\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'Phone 2'\"\r\n                               [(ngModel)]=\"editedPhone2\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
    }),
    tslib_1.__metadata("design:paramtypes", [CardViewUpdateService])
], CardViewPhoneItemComponent);
export { CardViewPhoneItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXBob25lLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvY2FyZC12aWV3L2NvbXBvbmVudHMvY2FyZC12aWV3LXBob25lLWl0ZW0vY2FyZC12aWV3LXBob25lLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQzNELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQU14RCxJQUFhLDBCQUEwQixHQUF2QyxNQUFhLDBCQUEwQjtJQXNCbkMsWUFBb0IscUJBQTRDO1FBQTVDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFoQmhFLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFHMUIsaUJBQVksR0FBWSxJQUFJLENBQUM7UUFRN0IsV0FBTSxHQUFZLEtBQUssQ0FBQztJQU14QixDQUFDO0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQy9DLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQ25ELENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQztJQUVELFlBQVk7UUFDUixPQUFPLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3pELENBQUM7SUFFRCxVQUFVO1FBQ04sT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQ25ELENBQUM7SUFFRCxXQUFXO1FBQ1AsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsT0FBTztRQUNILE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxTQUFTO1FBQ0wsT0FBTyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO0lBQzNELENBQUM7SUFFRCxXQUFXLENBQUMsVUFBbUI7UUFDM0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7UUFDekIsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDMUM7UUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDTixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ1osSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUMxQztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDL0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDL0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsTUFBTTtRQUNGLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRTtZQUN4RSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQzNDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7U0FDM0c7SUFDTCxDQUFDO0lBRUQsSUFBSSxZQUFZO1FBQ1osT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQztJQUN0QyxDQUFDO0lBRUQsT0FBTztRQUNILElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7Q0FDSixDQUFBOztZQXJFOEMscUJBQXFCOztBQW5CaEU7SUFEQyxLQUFLLEVBQUU7c0NBQ0Usc0JBQXNCOzREQUFDO0FBR2pDO0lBREMsS0FBSyxFQUFFOzs0REFDa0I7QUFHMUI7SUFEQyxLQUFLLEVBQUU7O2dFQUNxQjtBQUc3QjtJQURDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUM7OytEQUNoQjtBQUd6QjtJQURDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUM7OytEQUNoQjtBQWZoQiwwQkFBMEI7SUFKdEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLHNCQUFzQjtRQUNoQyxrbEpBQW9EO0tBQ3ZELENBQUM7NkNBdUI2QyxxQkFBcUI7R0F0QnZELDBCQUEwQixDQTJGdEM7U0EzRlksMEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzLCBPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1VwZGF0ZVNlcnZpY2UgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1Bob25lSXRlbU1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2NhcmQtdmlldy1waG9uZS1pdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgUGhvbmUgfSBmcm9tICcuLi8uLi8uLi8uLi9tb2RlbC9hZGRyZXNzLm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdjYXJkLXZpZXctcGhvbmUtaXRlbScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2FyZC12aWV3LXBob25lLWl0ZW0uY29tcG9uZW50Lmh0bWwnLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdQaG9uZUl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMsIE9uSW5pdCB7XHJcbiAgICBwaG9uZTogUGhvbmU7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgcHJvcGVydHk6IENhcmRWaWV3UGhvbmVJdGVtTW9kZWw7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGVkaXRhYmxlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGRpc3BsYXlFbXB0eTogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgncGhvbmUxSW5wdXQnLCB7c3RhdGljOiB0cnVlfSlcclxuICAgIHByaXZhdGUgcGhvbmUxSW5wdXQ6IGFueTtcclxuXHJcbiAgICBAVmlld0NoaWxkKCdwaG9uZTJJbnB1dCcsIHtzdGF0aWM6IHRydWV9KVxyXG4gICAgcHJpdmF0ZSBwaG9uZTJJbnB1dDogYW55O1xyXG5cclxuICAgIGluRWRpdDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgZWRpdGVkUGhvbmUxOiBzdHJpbmc7XHJcbiAgICBlZGl0ZWRQaG9uZTI6IHN0cmluZztcclxuICAgIGVycm9yTWVzc2FnZXM6IHN0cmluZ1tdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY2FyZFZpZXdVcGRhdGVTZXJ2aWNlOiBDYXJkVmlld1VwZGF0ZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcygpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmVkaXRlZFBob25lMSA9IHRoaXMucHJvcGVydHkudmFsdWUucGhvbmUxO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkUGhvbmUyID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5waG9uZTI7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd1Byb3BlcnR5KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRpc3BsYXlFbXB0eSB8fCAhdGhpcy5wcm9wZXJ0eS5pc0VtcHR5KCk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNFZGl0YWJsZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lZGl0YWJsZSAmJiB0aGlzLnByb3BlcnR5LmVkaXRhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzQ2xpY2thYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BlcnR5LmNsaWNrYWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNJY29uKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAhIXRoaXMucHJvcGVydHkuaWNvbjtcclxuICAgIH1cclxuXHJcbiAgICBoYXNFcnJvcnMoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lcnJvck1lc3NhZ2VzICYmIHRoaXMuZXJyb3JNZXNzYWdlcy5sZW5ndGg7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0RWRpdE1vZGUoZWRpdFN0YXR1czogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuaW5FZGl0ID0gZWRpdFN0YXR1cztcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucGhvbmUxSW5wdXQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucGhvbmUxSW5wdXQubmF0aXZlRWxlbWVudC5jbGljaygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMCk7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnBob25lMklucHV0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBob25lMklucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZWRpdGVkUGhvbmUxID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5waG9uZTE7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRQaG9uZTIgPSB0aGlzLnByb3BlcnR5LnZhbHVlLnBob25lMjtcclxuICAgICAgICB0aGlzLnNldEVkaXRNb2RlKGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcGVydHkuaXNWYWxpZChuZXcgUGhvbmUodGhpcy5lZGl0ZWRQaG9uZTEsIHRoaXMuZWRpdGVkUGhvbmUyKSkpIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJkVmlld1VwZGF0ZVNlcnZpY2UudXBkYXRlKHRoaXMucHJvcGVydHksXHJcbiAgICAgICAgICAgICAgICBuZXcgUGhvbmUodGhpcy5lZGl0ZWRQaG9uZTEsIHRoaXMuZWRpdGVkUGhvbmUyKSk7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcGVydHkudmFsdWUgPSBuZXcgUGhvbmUodGhpcy5lZGl0ZWRQaG9uZTEsIHRoaXMuZWRpdGVkUGhvbmUyKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRFZGl0TW9kZShmYWxzZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5lcnJvck1lc3NhZ2VzID0gdGhpcy5wcm9wZXJ0eS5nZXRWYWxpZGF0aW9uRXJyb3JzKG5ldyBQaG9uZSh0aGlzLmVkaXRlZFBob25lMSwgdGhpcy5lZGl0ZWRQaG9uZTIpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc3BsYXlWYWx1ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wZXJ0eS5kaXNwbGF5VmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgY2xpY2tlZCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS5jbGlja2VkKHRoaXMucHJvcGVydHkpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==