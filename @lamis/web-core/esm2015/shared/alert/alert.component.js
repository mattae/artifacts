import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { NotificationService } from '@alfresco/adf-core';
let AlertComponent = class AlertComponent {
    constructor(notification, alertService) {
        this.notification = notification;
        this.alertService = alertService;
    }
    ngOnInit() {
        this.alerts = this.alertService.get();
        this.alerts.forEach(alert => this.notification.openSnackMessage(alert.msg, 5000));
    }
    ngOnDestroy() {
        this.alerts = [];
    }
};
AlertComponent.ctorParameters = () => [
    { type: NotificationService },
    { type: JhiAlertService }
];
AlertComponent = tslib_1.__decorate([
    Component({
        selector: 'alert',
        template: ``
    }),
    tslib_1.__metadata("design:paramtypes", [NotificationService, JhiAlertService])
], AlertComponent);
export { AlertComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL2FsZXJ0L2FsZXJ0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBcUIsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUM5QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQU16RCxJQUFhLGNBQWMsR0FBM0IsTUFBYSxjQUFjO0lBR3ZCLFlBQW9CLFlBQWlDLEVBQVUsWUFBNkI7UUFBeEUsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBQVUsaUJBQVksR0FBWixZQUFZLENBQWlCO0lBQUcsQ0FBQztJQUVoRyxRQUFRO1FBQ0osSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDdEYsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0NBQ0osQ0FBQTs7WUFWcUMsbUJBQW1CO1lBQXdCLGVBQWU7O0FBSG5GLGNBQWM7SUFKMUIsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLE9BQU87UUFDakIsUUFBUSxFQUFFLEVBQUU7S0FDZixDQUFDOzZDQUlvQyxtQkFBbUIsRUFBd0IsZUFBZTtHQUhuRixjQUFjLENBYTFCO1NBYlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSmhpQWxlcnRTZXJ2aWNlIH0gZnJvbSAnbmctamhpcHN0ZXInO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhbGVydCcsXHJcbiAgICB0ZW1wbGF0ZTogYGBcclxufSlcclxuZXhwb3J0IGNsYXNzIEFsZXJ0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gICAgYWxlcnRzOiBhbnlbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uU2VydmljZSwgcHJpdmF0ZSBhbGVydFNlcnZpY2U6IEpoaUFsZXJ0U2VydmljZSkge31cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLmFsZXJ0cyA9IHRoaXMuYWxlcnRTZXJ2aWNlLmdldCgpO1xyXG4gICAgICAgIHRoaXMuYWxlcnRzLmZvckVhY2goYWxlcnQgPT4gdGhpcy5ub3RpZmljYXRpb24ub3BlblNuYWNrTWVzc2FnZShhbGVydC5tc2csIDUwMDApKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLmFsZXJ0cyA9IFtdO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==