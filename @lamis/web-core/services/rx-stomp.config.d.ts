export declare const RxStompConfig: {
    brokerURL: string;
    webSocketFactory: () => any;
    connectHeaders: {
        login: string;
        passcode: string;
    };
    heartbeatIncoming: number;
    heartbeatOutgoing: number;
};
