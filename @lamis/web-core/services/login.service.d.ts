import { AccountService } from '../core/auth/account.service';
import { AuthServerProvider } from '../core/auth/auth-jwt.service';
export declare class LoginService {
    private accountService;
    private authServerProvider;
    constructor(accountService: AccountService, authServerProvider: AuthServerProvider);
    login(credentials: any, callback?: any): Promise<{}>;
    loginWithToken(jwt: any, rememberMe: any): Promise<any>;
    logout(): void;
}
