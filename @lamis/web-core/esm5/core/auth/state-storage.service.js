import * as tslib_1 from "tslib";
import { Injectable, Injector } from '@angular/core';
import { SessionStorageService } from 'ngx-store';
import * as i0 from "@angular/core";
var StateStorageService = /** @class */ (function () {
    function StateStorageService(injector) {
        this.injector = injector;
        this.$sessionStorage = injector.get(SessionStorageService);
    }
    StateStorageService.prototype.getPreviousState = function () {
        return this.$sessionStorage.get('previousState');
    };
    StateStorageService.prototype.resetPreviousState = function () {
        this.$sessionStorage.remove('previousState');
    };
    StateStorageService.prototype.storePreviousState = function (previousStateName, previousStateParams) {
        var previousState = { name: previousStateName, params: previousStateParams };
        this.$sessionStorage.set('previousState', previousState);
    };
    StateStorageService.prototype.getDestinationState = function () {
        return this.$sessionStorage.get('destinationState');
    };
    StateStorageService.prototype.storeUrl = function (url) {
        this.$sessionStorage.set('previousUrl', url);
    };
    StateStorageService.prototype.getUrl = function () {
        return this.$sessionStorage.get('previousUrl');
    };
    StateStorageService.prototype.storeDestinationState = function (destinationState, destinationStateParams, fromState) {
        var destinationInfo = {
            destination: {
                name: destinationState.name,
                data: destinationState.data
            },
            params: destinationStateParams,
            from: {
                name: fromState.name
            }
        };
        this.$sessionStorage.set('destinationState', destinationInfo);
    };
    StateStorageService.ctorParameters = function () { return [
        { type: Injector }
    ]; };
    StateStorageService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function StateStorageService_Factory() { return new StateStorageService(i0.ɵɵinject(i0.INJECTOR)); }, token: StateStorageService, providedIn: "root" });
    StateStorageService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [Injector])
    ], StateStorageService);
    return StateStorageService;
}());
export { StateStorageService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGUtc3RvcmFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsiY29yZS9hdXRoL3N0YXRlLXN0b3JhZ2Uuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckQsT0FBTyxFQUF1QixxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQzs7QUFHdkU7SUFHSSw2QkFBb0IsUUFBa0I7UUFBbEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQyxJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQsOENBQWdCLEdBQWhCO1FBQ0ksT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsZ0RBQWtCLEdBQWxCO1FBQ0ksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELGdEQUFrQixHQUFsQixVQUFtQixpQkFBc0IsRUFBRSxtQkFBd0I7UUFDL0QsSUFBTSxhQUFhLEdBQUcsRUFBQyxJQUFJLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxFQUFFLG1CQUFtQixFQUFDLENBQUM7UUFDN0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLGFBQWEsQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFFRCxpREFBbUIsR0FBbkI7UUFDSSxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVELHNDQUFRLEdBQVIsVUFBUyxHQUFXO1FBQ2hCLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsb0NBQU0sR0FBTjtRQUNJLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVELG1EQUFxQixHQUFyQixVQUFzQixnQkFBcUIsRUFBRSxzQkFBMkIsRUFBRSxTQUFjO1FBQ3BGLElBQU0sZUFBZSxHQUFHO1lBQ3BCLFdBQVcsRUFBRTtnQkFDVCxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsSUFBSTtnQkFDM0IsSUFBSSxFQUFFLGdCQUFnQixDQUFDLElBQUk7YUFDOUI7WUFDRCxNQUFNLEVBQUUsc0JBQXNCO1lBQzlCLElBQUksRUFBRTtnQkFDRixJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUk7YUFDdkI7U0FDSixDQUFDO1FBQ0YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsZUFBZSxDQUFDLENBQUM7SUFDbEUsQ0FBQzs7Z0JBekM2QixRQUFROzs7SUFIN0IsbUJBQW1CO1FBRC9CLFVBQVUsQ0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMsQ0FBQztpREFJQyxRQUFRO09BSDdCLG1CQUFtQixDQTZDL0I7OEJBakREO0NBaURDLEFBN0NELElBNkNDO1NBN0NZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IExvY2FsU3RvcmFnZVNlcnZpY2UsIFNlc3Npb25TdG9yYWdlU2VydmljZSB9IGZyb20gJ25neC1zdG9yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcclxuZXhwb3J0IGNsYXNzIFN0YXRlU3RvcmFnZVNlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSAkc2Vzc2lvblN0b3JhZ2U6IFNlc3Npb25TdG9yYWdlU2VydmljZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGluamVjdG9yOiBJbmplY3Rvcikge1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlID0gaW5qZWN0b3IuZ2V0KFNlc3Npb25TdG9yYWdlU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJldmlvdXNTdGF0ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy4kc2Vzc2lvblN0b3JhZ2UuZ2V0KCdwcmV2aW91c1N0YXRlJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXRQcmV2aW91c1N0YXRlKCkge1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLnJlbW92ZSgncHJldmlvdXNTdGF0ZScpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3JlUHJldmlvdXNTdGF0ZShwcmV2aW91c1N0YXRlTmFtZTogYW55LCBwcmV2aW91c1N0YXRlUGFyYW1zOiBhbnkpIHtcclxuICAgICAgICBjb25zdCBwcmV2aW91c1N0YXRlID0ge25hbWU6IHByZXZpb3VzU3RhdGVOYW1lLCBwYXJhbXM6IHByZXZpb3VzU3RhdGVQYXJhbXN9O1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLnNldCgncHJldmlvdXNTdGF0ZScsIHByZXZpb3VzU3RhdGUpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlc3RpbmF0aW9uU3RhdGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuJHNlc3Npb25TdG9yYWdlLmdldCgnZGVzdGluYXRpb25TdGF0ZScpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3JlVXJsKHVybDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy4kc2Vzc2lvblN0b3JhZ2Uuc2V0KCdwcmV2aW91c1VybCcsIHVybCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXJsKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRzZXNzaW9uU3RvcmFnZS5nZXQoJ3ByZXZpb3VzVXJsJyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RvcmVEZXN0aW5hdGlvblN0YXRlKGRlc3RpbmF0aW9uU3RhdGU6IGFueSwgZGVzdGluYXRpb25TdGF0ZVBhcmFtczogYW55LCBmcm9tU3RhdGU6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IGRlc3RpbmF0aW9uSW5mbyA9IHtcclxuICAgICAgICAgICAgZGVzdGluYXRpb246IHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IGRlc3RpbmF0aW9uU3RhdGUubmFtZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGRlc3RpbmF0aW9uU3RhdGUuZGF0YVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBwYXJhbXM6IGRlc3RpbmF0aW9uU3RhdGVQYXJhbXMsXHJcbiAgICAgICAgICAgIGZyb206IHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IGZyb21TdGF0ZS5uYW1lXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLnNldCgnZGVzdGluYXRpb25TdGF0ZScsIGRlc3RpbmF0aW9uSW5mbyk7XHJcbiAgICB9XHJcbn1cclxuIl19