import * as tslib_1 from "tslib";
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { StateStorageService } from '../core/auth/state-storage.service';
import { LoginService } from './login.service';
import { JhiEventManager } from 'ng-jhipster';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
var LoginAuthenticationService = /** @class */ (function () {
    function LoginAuthenticationService(router, loginService, $localStorage, $sessionStorage, eventManager, stateStorageService, injector) {
        this.router = router;
        this.loginService = loginService;
        this.$localStorage = $localStorage;
        this.$sessionStorage = $sessionStorage;
        this.eventManager = eventManager;
        this.stateStorageService = stateStorageService;
        this.injector = injector;
        /*this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
        this.stateStorageService = this.injector.get(StateStorageService);
        this.eventManager = this.injector.get(JhiEventManager);*/
    }
    LoginAuthenticationService.prototype.setRedirect = function (value) {
    };
    LoginAuthenticationService.prototype.isEcmLoggedIn = function () {
        return false;
    };
    LoginAuthenticationService.prototype.isBpmLoggedIn = function () {
        return false;
    };
    LoginAuthenticationService.prototype.isOauth = function () {
        return false;
    };
    LoginAuthenticationService.prototype.getRedirect = function () {
        return null;
    };
    LoginAuthenticationService.prototype.login = function (username, password, rememberMe) {
        var _this = this;
        if (rememberMe === void 0) { rememberMe = false; }
        this.loginService
            .login({
            username: username,
            password: password,
            rememberMe: rememberMe
        })
            .then(function (data) {
            if (_this.router.url === '/account/register' || (/^\/account\/activate\//.test(_this.router.url)) ||
                (/^\/account\/reset\//.test(_this.router.url))) {
                _this.router.navigate(['']);
            }
            _this.eventManager.broadcast({
                name: 'authenticationSuccess',
                content: 'Sending Authentication Success'
            });
            // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // // since login is successful, go to stored previousState and clear previousState
            var redirect = _this.stateStorageService.getUrl();
            if (redirect) {
                _this.stateStorageService.storeUrl('');
                _this.router.navigate([redirect]);
            }
            else {
                _this.router.navigate(['/dashboard']);
            }
        });
        return of(true);
    };
    LoginAuthenticationService.ctorParameters = function () { return [
        { type: Router },
        { type: LoginService },
        { type: LocalStorageService },
        { type: SessionStorageService },
        { type: JhiEventManager },
        { type: StateStorageService },
        { type: Injector }
    ]; };
    LoginAuthenticationService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [Router,
            LoginService,
            LocalStorageService,
            SessionStorageService,
            JhiEventManager,
            StateStorageService,
            Injector])
    ], LoginAuthenticationService);
    return LoginAuthenticationService;
}());
export { LoginAuthenticationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2xvZ2luLWF1dGhlbnRpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQWMsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUd2RTtJQUVJLG9DQUFvQixNQUFjLEVBQ2QsWUFBMEIsRUFDMUIsYUFBa0MsRUFDbEMsZUFBc0MsRUFDdEMsWUFBNkIsRUFDN0IsbUJBQXdDLEVBQ3hDLFFBQWtCO1FBTmxCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBcUI7UUFDbEMsb0JBQWUsR0FBZixlQUFlLENBQXVCO1FBQ3RDLGlCQUFZLEdBQVosWUFBWSxDQUFpQjtRQUM3Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEM7OztpRUFHeUQ7SUFDN0QsQ0FBQztJQUVELGdEQUFXLEdBQVgsVUFBWSxLQUFVO0lBRXRCLENBQUM7SUFFRCxrREFBYSxHQUFiO1FBQ0ksT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELGtEQUFhLEdBQWI7UUFDSSxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsNENBQU8sR0FBUDtRQUNJLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxnREFBVyxHQUFYO1FBQ0ksT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELDBDQUFLLEdBQUwsVUFBTSxRQUFnQixFQUFFLFFBQWdCLEVBQUUsVUFBMkI7UUFBckUsaUJBMkJDO1FBM0J5QywyQkFBQSxFQUFBLGtCQUEyQjtRQUNqRSxJQUFJLENBQUMsWUFBWTthQUNaLEtBQUssQ0FBQztZQUNILFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFVBQVUsRUFBRSxVQUFVO1NBQ3pCLENBQUM7YUFDRCxJQUFJLENBQUMsVUFBQyxJQUFJO1lBQ1AsSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxtQkFBbUIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMzRixDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQy9DLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUM5QjtZQUNELEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUN4QixJQUFJLEVBQUUsdUJBQXVCO2dCQUM3QixPQUFPLEVBQUUsZ0NBQWdDO2FBQzVDLENBQUMsQ0FBQztZQUNILGlHQUFpRztZQUNqRyxtRkFBbUY7WUFDbkYsSUFBTSxRQUFRLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ25ELElBQUksUUFBUSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzthQUNwQztpQkFBTTtnQkFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7YUFDeEM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3BCLENBQUM7O2dCQTVEMkIsTUFBTTtnQkFDQSxZQUFZO2dCQUNYLG1CQUFtQjtnQkFDakIscUJBQXFCO2dCQUN4QixlQUFlO2dCQUNSLG1CQUFtQjtnQkFDOUIsUUFBUTs7SUFSN0IsMEJBQTBCO1FBRHRDLFVBQVUsRUFBRTtpREFHbUIsTUFBTTtZQUNBLFlBQVk7WUFDWCxtQkFBbUI7WUFDakIscUJBQXFCO1lBQ3hCLGVBQWU7WUFDUixtQkFBbUI7WUFDOUIsUUFBUTtPQVI3QiwwQkFBMEIsQ0ErRHRDO0lBQUQsaUNBQUM7Q0FBQSxBQS9ERCxJQStEQztTQS9EWSwwQkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBTdGF0ZVN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi4vY29yZS9hdXRoL3N0YXRlLXN0b3JhZ2Uuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ2luU2VydmljZSB9IGZyb20gJy4vbG9naW4uc2VydmljZSc7XHJcbmltcG9ydCB7IEpoaUV2ZW50TWFuYWdlciB9IGZyb20gJ25nLWpoaXBzdGVyJztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSwgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXN0b3JlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIExvZ2luQXV0aGVudGljYXRpb25TZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dpblNlcnZpY2U6IExvZ2luU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgJGxvY2FsU3RvcmFnZTogTG9jYWxTdG9yYWdlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgJHNlc3Npb25TdG9yYWdlOiBTZXNzaW9uU3RvcmFnZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGV2ZW50TWFuYWdlcjogSmhpRXZlbnRNYW5hZ2VyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBzdGF0ZVN0b3JhZ2VTZXJ2aWNlOiBTdGF0ZVN0b3JhZ2VTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IpIHtcclxuICAgICAgICAvKnRoaXMuJGxvY2FsU3RvcmFnZSA9IHRoaXMuaW5qZWN0b3IuZ2V0KExvY2FsU3RvcmFnZVNlcnZpY2UpO1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlID0gdGhpcy5pbmplY3Rvci5nZXQoU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlKTtcclxuICAgICAgICB0aGlzLnN0YXRlU3RvcmFnZVNlcnZpY2UgPSB0aGlzLmluamVjdG9yLmdldChTdGF0ZVN0b3JhZ2VTZXJ2aWNlKTtcclxuICAgICAgICB0aGlzLmV2ZW50TWFuYWdlciA9IHRoaXMuaW5qZWN0b3IuZ2V0KEpoaUV2ZW50TWFuYWdlcik7Ki9cclxuICAgIH1cclxuXHJcbiAgICBzZXRSZWRpcmVjdCh2YWx1ZTogYW55KXtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgaXNFY21Mb2dnZWRJbigpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNCcG1Mb2dnZWRJbigpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNPYXV0aCgpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmVkaXJlY3QoKSB7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgbG9naW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZywgcmVtZW1iZXJNZTogYm9vbGVhbiA9IGZhbHNlKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICB0aGlzLmxvZ2luU2VydmljZVxyXG4gICAgICAgICAgICAubG9naW4oe1xyXG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXJuYW1lLFxyXG4gICAgICAgICAgICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkLFxyXG4gICAgICAgICAgICAgICAgcmVtZW1iZXJNZTogcmVtZW1iZXJNZVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucm91dGVyLnVybCA9PT0gJy9hY2NvdW50L3JlZ2lzdGVyJyB8fCAoL15cXC9hY2NvdW50XFwvYWN0aXZhdGVcXC8vLnRlc3QodGhpcy5yb3V0ZXIudXJsKSkgfHxcclxuICAgICAgICAgICAgICAgICAgICAoL15cXC9hY2NvdW50XFwvcmVzZXRcXC8vLnRlc3QodGhpcy5yb3V0ZXIudXJsKSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJyddKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuZXZlbnRNYW5hZ2VyLmJyb2FkY2FzdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ2F1dGhlbnRpY2F0aW9uU3VjY2VzcycsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDogJ1NlbmRpbmcgQXV0aGVudGljYXRpb24gU3VjY2VzcydcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgLy8gLy8gcHJldmlvdXNTdGF0ZSB3YXMgc2V0IGluIHRoZSBhdXRoRXhwaXJlZEludGVyY2VwdG9yIGJlZm9yZSBiZWluZyByZWRpcmVjdGVkIHRvIGxvZ2luIG1vZGFsLlxyXG4gICAgICAgICAgICAgICAgLy8gLy8gc2luY2UgbG9naW4gaXMgc3VjY2Vzc2Z1bCwgZ28gdG8gc3RvcmVkIHByZXZpb3VzU3RhdGUgYW5kIGNsZWFyIHByZXZpb3VzU3RhdGVcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlZGlyZWN0ID0gdGhpcy5zdGF0ZVN0b3JhZ2VTZXJ2aWNlLmdldFVybCgpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlZGlyZWN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZVN0b3JhZ2VTZXJ2aWNlLnN0b3JlVXJsKCcnKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbcmVkaXJlY3RdKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvZGFzaGJvYXJkJ10pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gb2YodHJ1ZSk7XHJcbiAgICB9XHJcbn1cclxuIl19