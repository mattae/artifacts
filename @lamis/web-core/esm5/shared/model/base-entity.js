var Aggregate = /** @class */ (function () {
    function Aggregate(field, key) {
        this.field = field;
        this.key = key;
    }
    return Aggregate;
}());
export { Aggregate };
export function remove(array, element) {
    return array.filter(function (e) { return e.id !== element.id; });
}
export function clear(array) {
    array.length = 0;
    return array;
}
export function contains(array, element) {
    return array.filter(function (e) { return e.id === element.id; }).length > 0;
}
export function entityCompare(e1, e2) {
    return e1 && e2 ? e1.id == e2.id : e1 === e2;
}
export function enumCompare(e1, e2) {
    return (e1 !== undefined && e2 !== undefined) ? e1.valueOf() == e2.valueOf() : e1 === e2;
}
export function replace(array, element) {
    var result = remove(array, element);
    result.push(element);
    return result;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1lbnRpdHkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvbW9kZWwvYmFzZS1lbnRpdHkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBU0E7SUFDSSxtQkFBbUIsS0FBYSxFQUFTLEdBQVE7UUFBOUIsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUFTLFFBQUcsR0FBSCxHQUFHLENBQUs7SUFDakQsQ0FBQztJQUNMLGdCQUFDO0FBQUQsQ0FBQyxBQUhELElBR0M7O0FBR0QsTUFBTSxVQUFVLE1BQU0sQ0FBd0IsS0FBVSxFQUFFLE9BQVU7SUFDaEUsT0FBTyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxPQUFPLENBQUMsRUFBRSxFQUFuQixDQUFtQixDQUFDLENBQUM7QUFDbEQsQ0FBQztBQUVELE1BQU0sVUFBVSxLQUFLLENBQUksS0FBVTtJQUMvQixLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUNqQixPQUFPLEtBQUssQ0FBQztBQUNqQixDQUFDO0FBRUQsTUFBTSxVQUFVLFFBQVEsQ0FBd0IsS0FBVSxFQUFFLE9BQVU7SUFDbEUsT0FBTyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxPQUFPLENBQUMsRUFBRSxFQUFuQixDQUFtQixDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztBQUM3RCxDQUFDO0FBRUQsTUFBTSxVQUFVLGFBQWEsQ0FBd0IsRUFBSyxFQUFFLEVBQUs7SUFDN0QsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUE7QUFDaEQsQ0FBQztBQUVELE1BQU0sVUFBVSxXQUFXLENBQUMsRUFBTyxFQUFFLEVBQU87SUFDeEMsT0FBTyxDQUFDLEVBQUUsS0FBSyxTQUFTLElBQUksRUFBRSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDO0FBQzdGLENBQUM7QUFFRCxNQUFNLFVBQVUsT0FBTyxDQUF3QixLQUFVLEVBQUUsT0FBVTtJQUNqRSxJQUFJLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDckIsT0FBTyxNQUFNLENBQUM7QUFDbEIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgSUJhc2VFbnRpdHkge1xyXG4gICAgaWQ/OiBudW1iZXIgfCBzdHJpbmc7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgSUFnZ3JlZ2F0ZSB7XHJcbiAgICBmaWVsZD86IHN0cmluZztcclxuICAgIGtleT86IHN0cmluZztcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEFnZ3JlZ2F0ZSBpbXBsZW1lbnRzIElBZ2dyZWdhdGUge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZpZWxkOiBzdHJpbmcsIHB1YmxpYyBrZXk6IGFueSkge1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHJlbW92ZTxUIGV4dGVuZHMgSUJhc2VFbnRpdHk+KGFycmF5OiBUW10sIGVsZW1lbnQ6IFQpOiBUW10ge1xyXG4gICAgcmV0dXJuIGFycmF5LmZpbHRlcihlID0+IGUuaWQgIT09IGVsZW1lbnQuaWQpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gY2xlYXI8VD4oYXJyYXk6IFRbXSk6IFRbXSB7XHJcbiAgICBhcnJheS5sZW5ndGggPSAwO1xyXG4gICAgcmV0dXJuIGFycmF5O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gY29udGFpbnM8VCBleHRlbmRzIElCYXNlRW50aXR5PihhcnJheTogVFtdLCBlbGVtZW50OiBUKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gYXJyYXkuZmlsdGVyKGUgPT4gZS5pZCA9PT0gZWxlbWVudC5pZCkubGVuZ3RoID4gMDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGVudGl0eUNvbXBhcmU8VCBleHRlbmRzIElCYXNlRW50aXR5PihlMTogVCwgZTI6IFQpOiBib29sZWFuIHtcclxuICAgIHJldHVybiBlMSAmJiBlMiA/IGUxLmlkID09IGUyLmlkIDogZTEgPT09IGUyXHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBlbnVtQ29tcGFyZShlMTogYW55LCBlMjogYW55KTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gKGUxICE9PSB1bmRlZmluZWQgJiYgZTIgIT09IHVuZGVmaW5lZCkgPyBlMS52YWx1ZU9mKCkgPT0gZTIudmFsdWVPZigpIDogZTEgPT09IGUyO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gcmVwbGFjZTxUIGV4dGVuZHMgSUJhc2VFbnRpdHk+KGFycmF5OiBUW10sIGVsZW1lbnQ6IFQpOiBUW10ge1xyXG4gICAgbGV0IHJlc3VsdCA9IHJlbW92ZShhcnJheSwgZWxlbWVudCk7XHJcbiAgICByZXN1bHQucHVzaChlbGVtZW50KTtcclxuICAgIHJldHVybiByZXN1bHQ7XHJcbn1cclxuIl19