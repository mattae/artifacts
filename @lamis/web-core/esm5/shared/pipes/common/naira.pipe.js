import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
import { CurrencyPipe } from "@angular/common";
var NairaPipe = /** @class */ (function () {
    function NairaPipe() {
        this.pipe = new CurrencyPipe('en');
    }
    NairaPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return this.pipe.transform(value, '₦');
    };
    NairaPipe = tslib_1.__decorate([
        Pipe({
            name: 'naira'
        })
    ], NairaPipe);
    return NairaPipe;
}());
export { NairaPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmFpcmEucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9waXBlcy9jb21tb24vbmFpcmEucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBSy9DO0lBSEE7UUFJWSxTQUFJLEdBQUcsSUFBSSxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7SUFLMUMsQ0FBQztJQUhHLDZCQUFTLEdBQVQsVUFBVSxLQUFVO1FBQUUsY0FBYzthQUFkLFVBQWMsRUFBZCxxQkFBYyxFQUFkLElBQWM7WUFBZCw2QkFBYzs7UUFDaEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUxRLFNBQVM7UUFIckIsSUFBSSxDQUFDO1lBQ0YsSUFBSSxFQUFFLE9BQU87U0FDaEIsQ0FBQztPQUNXLFNBQVMsQ0FNckI7SUFBRCxnQkFBQztDQUFBLEFBTkQsSUFNQztTQU5ZLFNBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQ3VycmVuY3lQaXBlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5cclxuQFBpcGUoe1xyXG4gICAgbmFtZTogJ25haXJhJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmFpcmFQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcbiAgICBwcml2YXRlIHBpcGUgPSBuZXcgQ3VycmVuY3lQaXBlKCdlbicpO1xyXG5cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogYW55LCAuLi5hcmdzOiBhbnlbXSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnBpcGUudHJhbnNmb3JtKHZhbHVlLCAn4oKmJyk7XHJcbiAgICB9XHJcbn1cclxuIl19