import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
var ExcerptPipe = /** @class */ (function () {
    function ExcerptPipe() {
    }
    ExcerptPipe.prototype.transform = function (text, limit) {
        if (limit === void 0) { limit = 5; }
        if (text.length <= limit)
            return text;
        return text.substring(0, limit) + '...';
    };
    ExcerptPipe = tslib_1.__decorate([
        Pipe({ name: 'excerpt' })
    ], ExcerptPipe);
    return ExcerptPipe;
}());
export { ExcerptPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhjZXJwdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3BpcGVzL2NvbW1vbi9leGNlcnB0LnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBR3BEO0lBQUE7SUFNQSxDQUFDO0lBTEcsK0JBQVMsR0FBVCxVQUFVLElBQVksRUFBRSxLQUFpQjtRQUFqQixzQkFBQSxFQUFBLFNBQWlCO1FBQ3JDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLO1lBQ3BCLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDO0lBQzVDLENBQUM7SUFMUSxXQUFXO1FBRHZCLElBQUksQ0FBQyxFQUFDLElBQUksRUFBRSxTQUFTLEVBQUMsQ0FBQztPQUNYLFdBQVcsQ0FNdkI7SUFBRCxrQkFBQztDQUFBLEFBTkQsSUFNQztTQU5ZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbkBQaXBlKHtuYW1lOiAnZXhjZXJwdCd9KVxyXG5leHBvcnQgY2xhc3MgRXhjZXJwdFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICAgIHRyYW5zZm9ybSh0ZXh0OiBzdHJpbmcsIGxpbWl0OiBudW1iZXIgPSA1KSB7XHJcbiAgICAgICAgaWYgKHRleHQubGVuZ3RoIDw9IGxpbWl0KVxyXG4gICAgICAgICAgICByZXR1cm4gdGV4dDtcclxuICAgICAgICByZXR1cm4gdGV4dC5zdWJzdHJpbmcoMCwgbGltaXQpICsgJy4uLic7XHJcbiAgICB9XHJcbn1cclxuIl19