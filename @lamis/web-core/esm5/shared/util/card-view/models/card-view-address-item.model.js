import * as tslib_1 from "tslib";
import { CardViewBaseItemModel } from '@alfresco/adf-core';
var CardViewAddressItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewAddressItemModel, _super);
    function CardViewAddressItemModel(obj) {
        var _this = _super.call(this, obj) || this;
        _this.type = 'address';
        return _this;
    }
    Object.defineProperty(CardViewAddressItemModel.prototype, "displayValue", {
        get: function () {
            if (this.isEmpty()) {
                return this.default;
            }
            else {
                return this.getValue();
            }
        },
        enumerable: true,
        configurable: true
    });
    CardViewAddressItemModel.prototype.getValue = function () {
        return ("" + this.value.street1 + (!!this.value.street2 ? ', ' + this.value.street2 : '') + "\n            " + (!!this.value.city ? ', ' + this.value.city : '') + (!!this.value.lga ? ', ' + this.value.lga.name : ''))
            .trim();
    };
    return CardViewAddressItemModel;
}(CardViewBaseItemModel));
export { CardViewAddressItemModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWFkZHJlc3MtaXRlbS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2NhcmQtdmlldy9tb2RlbHMvY2FyZC12aWV3LWFkZHJlc3MtaXRlbS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLHFCQUFxQixFQUF1QyxNQUFNLG9CQUFvQixDQUFDO0FBR2hHO0lBQThDLG9EQUFxQjtJQUcvRCxrQ0FBWSxHQUFrQztRQUE5QyxZQUNJLGtCQUFNLEdBQUcsQ0FBQyxTQUNiO1FBSkQsVUFBSSxHQUFXLFNBQVMsQ0FBQzs7SUFJekIsQ0FBQztJQUVELHNCQUFJLGtEQUFZO2FBQWhCO1lBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ2hCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxPQUFPLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUMxQjtRQUNMLENBQUM7OztPQUFBO0lBRUQsMkNBQVEsR0FBUjtRQUNJLE9BQU8sQ0FBQSxLQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxJQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLHdCQUM5RSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFFLENBQUE7YUFDdkcsSUFBSSxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUNMLCtCQUFDO0FBQUQsQ0FBQyxBQXBCRCxDQUE4QyxxQkFBcUIsR0FvQmxFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsLCBDYXJkVmlld0l0ZW0sIER5bmFtaWNDb21wb25lbnRNb2RlbCB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcbmltcG9ydCB7IENhcmRJdGVtQWRkcmVzc0l0ZW1Qcm9wZXJ0aWVzIH0gZnJvbSAnLi9jYXJkLWl0ZW0tYWRkcmVzcy1pdGVtLnByb3BlcnRpZXMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3QWRkcmVzc0l0ZW1Nb2RlbCBleHRlbmRzIENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCBpbXBsZW1lbnRzIENhcmRWaWV3SXRlbSwgRHluYW1pY0NvbXBvbmVudE1vZGVsIHtcclxuICAgIHR5cGU6IHN0cmluZyA9ICdhZGRyZXNzJztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo6IENhcmRJdGVtQWRkcmVzc0l0ZW1Qcm9wZXJ0aWVzKSB7XHJcbiAgICAgICAgc3VwZXIob2JqKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzcGxheVZhbHVlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzRW1wdHkoKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5kZWZhdWx0O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmdldFZhbHVlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFZhbHVlKCkge1xyXG4gICAgICAgIHJldHVybiBgJHt0aGlzLnZhbHVlLnN0cmVldDF9JHshIXRoaXMudmFsdWUuc3RyZWV0MiA/ICcsICcgKyB0aGlzLnZhbHVlLnN0cmVldDIgOiAnJ31cclxuICAgICAgICAgICAgJHshIXRoaXMudmFsdWUuY2l0eSA/ICcsICcgKyB0aGlzLnZhbHVlLmNpdHkgOiAnJ30keyEhdGhpcy52YWx1ZS5sZ2EgPyAnLCAnICsgdGhpcy52YWx1ZS5sZ2EubmFtZSA6ICcnfWBcclxuICAgICAgICAgICAgLnRyaW0oKTtcclxuICAgIH1cclxufSJdfQ==