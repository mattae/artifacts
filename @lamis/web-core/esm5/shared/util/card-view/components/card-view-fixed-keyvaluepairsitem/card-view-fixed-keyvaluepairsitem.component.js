/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as tslib_1 from "tslib";
import { MatTableDataSource } from '@angular/material';
import { Component, Input } from '@angular/core';
import { CardViewKeyValuePairsItemType, CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewFixedKeyValuePairsItemModel } from '../../models/card-view-fixed-keyvaluepairs.model';
var CardViewFixedKeyvaluepairsitemComponent = /** @class */ (function () {
    function CardViewFixedKeyvaluepairsitemComponent(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
    }
    CardViewFixedKeyvaluepairsitemComponent.prototype.ngOnChanges = function () {
        this.values = this.property.value || [];
        this.matTableValues = new MatTableDataSource(this.values);
    };
    CardViewFixedKeyvaluepairsitemComponent.prototype.isEditable = function () {
        return this.editable && this.property.editable;
    };
    CardViewFixedKeyvaluepairsitemComponent.prototype.add = function () {
        this.values.push({ name: '', value: '' });
    };
    CardViewFixedKeyvaluepairsitemComponent.prototype.remove = function (index) {
        this.values.splice(index, 1);
        this.save(true);
    };
    CardViewFixedKeyvaluepairsitemComponent.prototype.onBlur = function (value) {
        if (value.length) {
            this.save();
        }
    };
    CardViewFixedKeyvaluepairsitemComponent.prototype.save = function (remove) {
        var validValues = this.values.filter(function (i) { return i.name.length && i.value.length; });
        if (remove || validValues.length) {
            this.cardViewUpdateService.update(this.property, validValues);
            this.property.value = validValues;
        }
    };
    CardViewFixedKeyvaluepairsitemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", CardViewFixedKeyValuePairsItemModel)
    ], CardViewFixedKeyvaluepairsitemComponent.prototype, "property", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], CardViewFixedKeyvaluepairsitemComponent.prototype, "editable", void 0);
    CardViewFixedKeyvaluepairsitemComponent = tslib_1.__decorate([
        Component({
            selector: 'card-view-keyvaluepair',
            template: "<div [attr.data-automation-id]=\"'card-key-value-pairs-label-' + property.key\" class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n\r\n    <div *ngIf=\"!isEditable()\" class=\"card-view__key-value-pairs__read-only\">\r\n        <mat-table #table [dataSource]=\"matTableValues\" class=\"mat-elevation-z8\">\r\n            <ng-container matColumnDef=\"name\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.name}}</mat-cell>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"value\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.value}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <mat-header-row *matHeaderRowDef=\"['name', 'value']\"></mat-header-row>\r\n            <mat-row *matRowDef=\"let row; columns: ['name', 'value'];\"></mat-row>\r\n        </mat-table>\r\n    </div>\r\n\r\n\r\n    <div class=\"card-view__key-value-pairs\" *ngIf=\"isEditable() && values && values.length\">\r\n        <div class=\"card-view__key-value-pairs__row\">\r\n            <div class=\"card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</div>\r\n            <div class=\"card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</div>\r\n        </div>\r\n\r\n        <div class=\"card-view__key-value-pairs__row\" *ngFor=\"let item of values; let i = index\">\r\n            <div class=\"card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput\r\n                           [disabled]=\"true\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-name-input-' + i\"\r\n                           [(ngModel)]=\"values[i].name\">\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput\r\n                           (blur)=\"onBlur(item.value)\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-value-input-' + i\"\r\n                           [(ngModel)]=\"values[i].value\">\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
            styles: [".card-view__key-value-pairs__col{display:inline-block;width:39%}.card-view__key-value-pairs__col .mat-form-field{width:100%}.card-view__key-value-pairs__read-only .mat-table{box-shadow:none}.card-view__key-value-pairs__read-only .mat-header-row,.card-view__key-value-pairs__read-only .mat-row{padding:0}"]
        }),
        tslib_1.__metadata("design:paramtypes", [CardViewUpdateService])
    ], CardViewFixedKeyvaluepairsitemComponent);
    return CardViewFixedKeyvaluepairsitemComponent;
}());
export { CardViewFixedKeyvaluepairsitemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWZpeGVkLWtleXZhbHVlcGFpcnNpdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2NhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1maXhlZC1rZXl2YWx1ZXBhaXJzaXRlbS9jYXJkLXZpZXctZml4ZWQta2V5dmFsdWVwYWlyc2l0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7R0FlRzs7QUFFSCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUM1RCxPQUFPLEVBQUUsNkJBQTZCLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUMxRixPQUFPLEVBQUUsbUNBQW1DLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQVF2RztJQVdJLGlEQUFvQixxQkFBNEM7UUFBNUMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUxoRSxhQUFRLEdBQVksS0FBSyxDQUFDO0lBS3lDLENBQUM7SUFFcEUsNkRBQVcsR0FBWDtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELDREQUFVLEdBQVY7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQztJQUVELHFEQUFHLEdBQUg7UUFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELHdEQUFNLEdBQU4sVUFBTyxLQUFhO1FBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3BCLENBQUM7SUFFRCx3REFBTSxHQUFOLFVBQU8sS0FBSztRQUNSLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNkLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVELHNEQUFJLEdBQUosVUFBSyxNQUFnQjtRQUNqQixJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUEvQixDQUErQixDQUFDLENBQUM7UUFFN0UsSUFBSSxNQUFNLElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRTtZQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDO1NBQ3JDO0lBQ0wsQ0FBQzs7Z0JBakMwQyxxQkFBcUI7O0lBUmhFO1FBREMsS0FBSyxFQUFFOzBDQUNFLG1DQUFtQzs2RUFBQztJQUc5QztRQURDLEtBQUssRUFBRTs7NkVBQ2tCO0lBTmpCLHVDQUF1QztRQU5uRCxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsd0JBQXdCO1lBQ2xDLG1sRkFBMkQ7O1NBRTlELENBQUM7aURBYTZDLHFCQUFxQjtPQVh2RCx1Q0FBdUMsQ0E2Q25EO0lBQUQsOENBQUM7Q0FBQSxBQTdDRCxJQTZDQztTQTdDWSx1Q0FBdUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTYgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgTWF0VGFibGVEYXRhU291cmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdLZXlWYWx1ZVBhaXJzSXRlbVR5cGUsIENhcmRWaWV3VXBkYXRlU2VydmljZSB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3Rml4ZWRLZXlWYWx1ZVBhaXJzSXRlbU1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2NhcmQtdmlldy1maXhlZC1rZXl2YWx1ZXBhaXJzLm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdjYXJkLXZpZXcta2V5dmFsdWVwYWlyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jYXJkLXZpZXcta2V5dmFsdWVwYWlyc2l0ZW0uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vY2FyZC12aWV3LWtleXZhbHVlcGFpcnNpdGVtLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld0ZpeGVkS2V5dmFsdWVwYWlyc2l0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBwcm9wZXJ0eTogQ2FyZFZpZXdGaXhlZEtleVZhbHVlUGFpcnNJdGVtTW9kZWw7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGVkaXRhYmxlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgdmFsdWVzOiBDYXJkVmlld0tleVZhbHVlUGFpcnNJdGVtVHlwZVtdO1xyXG4gICAgbWF0VGFibGVWYWx1ZXM6IE1hdFRhYmxlRGF0YVNvdXJjZTxDYXJkVmlld0tleVZhbHVlUGFpcnNJdGVtVHlwZT47XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjYXJkVmlld1VwZGF0ZVNlcnZpY2U6IENhcmRWaWV3VXBkYXRlU2VydmljZSkge31cclxuXHJcbiAgICBuZ09uQ2hhbmdlcygpIHtcclxuICAgICAgICB0aGlzLnZhbHVlcyA9IHRoaXMucHJvcGVydHkudmFsdWUgfHwgW107XHJcbiAgICAgICAgdGhpcy5tYXRUYWJsZVZhbHVlcyA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UodGhpcy52YWx1ZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIGlzRWRpdGFibGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWRpdGFibGUgJiYgdGhpcy5wcm9wZXJ0eS5lZGl0YWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBhZGQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy52YWx1ZXMucHVzaCh7IG5hbWU6ICcnLCB2YWx1ZTogJycgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlKGluZGV4OiBudW1iZXIpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnZhbHVlcy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIHRoaXMuc2F2ZSh0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkJsdXIodmFsdWUpOiB2b2lkIHtcclxuICAgICAgICBpZiAodmFsdWUubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzYXZlKHJlbW92ZT86IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgICAgICBjb25zdCB2YWxpZFZhbHVlcyA9IHRoaXMudmFsdWVzLmZpbHRlcihpID0+IGkubmFtZS5sZW5ndGggJiYgaS52YWx1ZS5sZW5ndGgpO1xyXG5cclxuICAgICAgICBpZiAocmVtb3ZlIHx8IHZhbGlkVmFsdWVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS51cGRhdGUodGhpcy5wcm9wZXJ0eSwgdmFsaWRWYWx1ZXMpO1xyXG4gICAgICAgICAgICB0aGlzLnByb3BlcnR5LnZhbHVlID0gdmFsaWRWYWx1ZXM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==