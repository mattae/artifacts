import * as tslib_1 from "tslib";
import { Component, Input, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewNameItemModel } from '../../models/card-view-name-item.model';
import { PersonName } from '../../../../model/address.model';
var CardViewNameItemComponent = /** @class */ (function () {
    function CardViewNameItemComponent(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    CardViewNameItemComponent.prototype.ngOnChanges = function () {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
    };
    CardViewNameItemComponent.prototype.showProperty = function () {
        return this.displayEmpty || !this.property.isEmpty();
    };
    CardViewNameItemComponent.prototype.isEditable = function () {
        return this.editable && this.property.editable;
    };
    CardViewNameItemComponent.prototype.isClickable = function () {
        return this.property.clickable;
    };
    CardViewNameItemComponent.prototype.hasIcon = function () {
        return !!this.property.icon;
    };
    CardViewNameItemComponent.prototype.hasErrors = function () {
        return this.errorMessages && this.errorMessages.length;
    };
    CardViewNameItemComponent.prototype.setEditMode = function (editStatus) {
        var _this = this;
        this.inEdit = editStatus;
        setTimeout(function () {
            if (_this.titleInput) {
                _this.titleInput.nativeElement.click();
            }
        }, 0);
        setTimeout(function () {
            if (_this.firstNameInput) {
                _this.firstNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(function () {
            if (_this.middleNameInput) {
                _this.middleNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(function () {
            if (_this.surnameInput) {
                _this.surnameInput.nativeElement.click();
            }
        }, 0);
    };
    CardViewNameItemComponent.prototype.reset = function () {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
        this.setEditMode(false);
    };
    CardViewNameItemComponent.prototype.update = function () {
        console.log('Property', this.property);
        if (this.property.isValid(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname))) {
            this.cardViewUpdateService.update(this.property, new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
            this.property.value = new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
        }
    };
    Object.defineProperty(CardViewNameItemComponent.prototype, "displayValue", {
        get: function () {
            return this.property.displayValue;
        },
        enumerable: true,
        configurable: true
    });
    CardViewNameItemComponent.prototype.clicked = function () {
        this.cardViewUpdateService.clicked(this.property);
    };
    CardViewNameItemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", CardViewNameItemModel)
    ], CardViewNameItemComponent.prototype, "property", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], CardViewNameItemComponent.prototype, "editable", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], CardViewNameItemComponent.prototype, "displayEmpty", void 0);
    tslib_1.__decorate([
        ViewChild('titleInput', { static: true }),
        tslib_1.__metadata("design:type", Object)
    ], CardViewNameItemComponent.prototype, "titleInput", void 0);
    tslib_1.__decorate([
        ViewChild('firstNameInput', { static: true }),
        tslib_1.__metadata("design:type", Object)
    ], CardViewNameItemComponent.prototype, "firstNameInput", void 0);
    tslib_1.__decorate([
        ViewChild('middleNameInput', { static: true }),
        tslib_1.__metadata("design:type", Object)
    ], CardViewNameItemComponent.prototype, "middleNameInput", void 0);
    tslib_1.__decorate([
        ViewChild('titleInput', { static: true }),
        tslib_1.__metadata("design:type", Object)
    ], CardViewNameItemComponent.prototype, "surnameInput", void 0);
    CardViewNameItemComponent = tslib_1.__decorate([
        Component({
            selector: 'card-view-name-item',
            template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #titleInput\r\n                               matInput\r\n                               [placeholder]=\"'Title'\"\r\n                               [(ngModel)]=\"editedTitle\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #firstNameInput\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'First name'\"\r\n                               [(ngModel)]=\"editedFirstName\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #middleNameInput\r\n                               matInput\r\n                               [placeholder]=\"'Middle name'\"\r\n                               [(ngModel)]=\"editedMiddleName\"\r\n                               [attr.data-automation-id]=\"'card-textitem-middlenameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #surnameInput\r\n                               matInput\r\n                               [placeholder]=\"'Surname'\"\r\n                               [(ngModel)]=\"editedSurname\"\r\n                               [attr.data-automation-id]=\"'card-textitem-surnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
        }),
        tslib_1.__metadata("design:paramtypes", [CardViewUpdateService])
    ], CardViewNameItemComponent);
    return CardViewNameItemComponent;
}());
export { CardViewNameItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LW5hbWUtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvY29tcG9uZW50cy9jYXJkLXZpZXctbmFtZS1pdGVtL2NhcmQtdmlldy1uYW1lLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDM0QsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDL0UsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBTTdEO0lBNkJJLG1DQUFvQixxQkFBNEM7UUFBNUMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQXhCaEUsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUcxQixpQkFBWSxHQUFZLElBQUksQ0FBQztRQWM3QixXQUFNLEdBQVksS0FBSyxDQUFDO0lBUXhCLENBQUM7SUFFRCwrQ0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDN0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFDckQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztJQUNyRCxDQUFDO0lBRUQsZ0RBQVksR0FBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDekQsQ0FBQztJQUVELDhDQUFVLEdBQVY7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQztJQUVELCtDQUFXLEdBQVg7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO0lBQ25DLENBQUM7SUFFRCwyQ0FBTyxHQUFQO1FBQ0ksT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7SUFDaEMsQ0FBQztJQUVELDZDQUFTLEdBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7SUFDM0QsQ0FBQztJQUVELCtDQUFXLEdBQVgsVUFBWSxVQUFtQjtRQUEvQixpQkFzQkM7UUFyQkcsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7UUFDekIsVUFBVSxDQUFDO1lBQ1AsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUN6QztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNOLFVBQVUsQ0FBQztZQUNQLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDN0M7UUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDTixVQUFVLENBQUM7WUFDUCxJQUFJLEtBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3RCLEtBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQzlDO1FBQ0wsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ04sVUFBVSxDQUFDO1lBQ1AsSUFBSSxLQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNuQixLQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUMzQztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7SUFFRCx5Q0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDN0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFDckQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFRCwwQ0FBTSxHQUFOO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRTtZQUMxSCxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQzNDLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDdkcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDeEgsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7U0FDN0o7SUFDTCxDQUFDO0lBRUQsc0JBQUksbURBQVk7YUFBaEI7WUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDO1FBQ3RDLENBQUM7OztPQUFBO0lBRUQsMkNBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7O2dCQWhGMEMscUJBQXFCOztJQTNCaEU7UUFEQyxLQUFLLEVBQUU7MENBQ0UscUJBQXFCOytEQUFDO0lBR2hDO1FBREMsS0FBSyxFQUFFOzsrREFDa0I7SUFHMUI7UUFEQyxLQUFLLEVBQUU7O21FQUNxQjtJQUc3QjtRQURDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUM7O2lFQUNoQjtJQUd4QjtRQURDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQzs7cUVBQ2hCO0lBRzVCO1FBREMsU0FBUyxDQUFDLGlCQUFpQixFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDOztzRUFDaEI7SUFHN0I7UUFEQyxTQUFTLENBQUMsWUFBWSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDOzttRUFDZDtJQXBCakIseUJBQXlCO1FBSnJDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxxQkFBcUI7WUFDL0Isb3BMQUFtRDtTQUN0RCxDQUFDO2lEQThCNkMscUJBQXFCO09BN0J2RCx5QkFBeUIsQ0E4R3JDO0lBQUQsZ0NBQUM7Q0FBQSxBQTlHRCxJQThHQztTQTlHWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkNoYW5nZXMsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1VwZGF0ZVNlcnZpY2UgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld05hbWVJdGVtTW9kZWwgfSBmcm9tICcuLi8uLi9tb2RlbHMvY2FyZC12aWV3LW5hbWUtaXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IFBlcnNvbk5hbWUgfSBmcm9tICcuLi8uLi8uLi8uLi9tb2RlbC9hZGRyZXNzLm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdjYXJkLXZpZXctbmFtZS1pdGVtJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jYXJkLXZpZXctbmFtZS1pdGVtLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdOYW1lSXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgcHJvcGVydHk6IENhcmRWaWV3TmFtZUl0ZW1Nb2RlbDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZWRpdGFibGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZGlzcGxheUVtcHR5OiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICBAVmlld0NoaWxkKCd0aXRsZUlucHV0Jywge3N0YXRpYzogdHJ1ZX0pXHJcbiAgICBwcml2YXRlIHRpdGxlSW5wdXQ6IGFueTtcclxuXHJcbiAgICBAVmlld0NoaWxkKCdmaXJzdE5hbWVJbnB1dCcsIHtzdGF0aWM6IHRydWV9KVxyXG4gICAgcHJpdmF0ZSBmaXJzdE5hbWVJbnB1dDogYW55O1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ21pZGRsZU5hbWVJbnB1dCcsIHtzdGF0aWM6IHRydWV9KVxyXG4gICAgcHJpdmF0ZSBtaWRkbGVOYW1lSW5wdXQ6IGFueTtcclxuXHJcbiAgICBAVmlld0NoaWxkKCd0aXRsZUlucHV0Jywge3N0YXRpYzogdHJ1ZX0pXHJcbiAgICBwcml2YXRlIHN1cm5hbWVJbnB1dDogYW55O1xyXG5cclxuICAgIGluRWRpdDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgZWRpdGVkVGl0bGU6IHN0cmluZztcclxuICAgIGVkaXRlZFN1cm5hbWU6IHN0cmluZztcclxuICAgIGVkaXRlZEZpcnN0TmFtZTogc3RyaW5nO1xyXG4gICAgZWRpdGVkTWlkZGxlTmFtZTogc3RyaW5nO1xyXG4gICAgZXJyb3JNZXNzYWdlczogc3RyaW5nW107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjYXJkVmlld1VwZGF0ZVNlcnZpY2U6IENhcmRWaWV3VXBkYXRlU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZWRpdGVkVGl0bGUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLnRpdGxlO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkRmlyc3ROYW1lID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5maXJzdE5hbWU7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRNaWRkbGVOYW1lID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5taWRkbGVOYW1lO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkU3VybmFtZSA9IHRoaXMucHJvcGVydHkudmFsdWUuc3VybmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBzaG93UHJvcGVydHkoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGlzcGxheUVtcHR5IHx8ICF0aGlzLnByb3BlcnR5LmlzRW1wdHkoKTtcclxuICAgIH1cclxuXHJcbiAgICBpc0VkaXRhYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVkaXRhYmxlICYmIHRoaXMucHJvcGVydHkuZWRpdGFibGU7XHJcbiAgICB9XHJcblxyXG4gICAgaXNDbGlja2FibGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucHJvcGVydHkuY2xpY2thYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc0ljb24oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICEhdGhpcy5wcm9wZXJ0eS5pY29uO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc0Vycm9ycygpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVycm9yTWVzc2FnZXMgJiYgdGhpcy5lcnJvck1lc3NhZ2VzLmxlbmd0aDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRFZGl0TW9kZShlZGl0U3RhdHVzOiBib29sZWFuKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5pbkVkaXQgPSBlZGl0U3RhdHVzO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy50aXRsZUlucHV0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRpdGxlSW5wdXQubmF0aXZlRWxlbWVudC5jbGljaygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMCk7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZpcnN0TmFtZUlucHV0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpcnN0TmFtZUlucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5taWRkbGVOYW1lSW5wdXQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubWlkZGxlTmFtZUlucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdXJuYW1lSW5wdXQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3VybmFtZUlucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZWRpdGVkVGl0bGUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLnRpdGxlO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkRmlyc3ROYW1lID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5maXJzdE5hbWU7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRNaWRkbGVOYW1lID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5taWRkbGVOYW1lO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkU3VybmFtZSA9IHRoaXMucHJvcGVydHkudmFsdWUuc3VybmFtZTtcclxuICAgICAgICB0aGlzLnNldEVkaXRNb2RlKGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoKTogdm9pZCB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ1Byb3BlcnR5JywgdGhpcy5wcm9wZXJ0eSk7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcGVydHkuaXNWYWxpZChuZXcgUGVyc29uTmFtZSh0aGlzLmVkaXRlZFRpdGxlLCB0aGlzLmVkaXRlZEZpcnN0TmFtZSwgdGhpcy5lZGl0ZWRNaWRkbGVOYW1lLCB0aGlzLmVkaXRlZFN1cm5hbWUpKSkge1xyXG4gICAgICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS51cGRhdGUodGhpcy5wcm9wZXJ0eSxcclxuICAgICAgICAgICAgICAgIG5ldyBQZXJzb25OYW1lKHRoaXMuZWRpdGVkVGl0bGUsIHRoaXMuZWRpdGVkRmlyc3ROYW1lLCB0aGlzLmVkaXRlZE1pZGRsZU5hbWUsIHRoaXMuZWRpdGVkU3VybmFtZSkpO1xyXG4gICAgICAgICAgICB0aGlzLnByb3BlcnR5LnZhbHVlID0gbmV3IFBlcnNvbk5hbWUodGhpcy5lZGl0ZWRUaXRsZSwgdGhpcy5lZGl0ZWRGaXJzdE5hbWUsIHRoaXMuZWRpdGVkTWlkZGxlTmFtZSwgdGhpcy5lZGl0ZWRTdXJuYW1lKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRFZGl0TW9kZShmYWxzZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5lcnJvck1lc3NhZ2VzID0gdGhpcy5wcm9wZXJ0eS5nZXRWYWxpZGF0aW9uRXJyb3JzKG5ldyBQZXJzb25OYW1lKHRoaXMuZWRpdGVkVGl0bGUsIHRoaXMuZWRpdGVkRmlyc3ROYW1lLCB0aGlzLmVkaXRlZE1pZGRsZU5hbWUsIHRoaXMuZWRpdGVkU3VybmFtZSkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzcGxheVZhbHVlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BlcnR5LmRpc3BsYXlWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBjbGlja2VkKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY2FyZFZpZXdVcGRhdGVTZXJ2aWNlLmNsaWNrZWQodGhpcy5wcm9wZXJ0eSk7XHJcbiAgICB9XHJcbn1cclxuIl19