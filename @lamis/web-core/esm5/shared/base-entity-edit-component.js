import * as tslib_1 from "tslib";
import { ViewChild } from '@angular/core';
import { MatButton, MatProgressBar } from '@angular/material';
var BaseEntityEditComponent = /** @class */ (function () {
    function BaseEntityEditComponent(notification, route) {
        this.notification = notification;
        this.route = route;
        this.error = false;
    }
    BaseEntityEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isSaving = false;
        this.route.data.subscribe(function (_a) {
            var entity = _a.entity;
            _this.entity = !!entity && entity.body ? entity.body : entity;
        });
        if (this.entity === undefined) {
            this.entity = this.createEntity();
        }
    };
    BaseEntityEditComponent.prototype.previousState = function () {
        window.history.back();
    };
    BaseEntityEditComponent.prototype.save = function (valid) {
        this.submitButton.disabled = true;
        this.progressBar.mode = 'indeterminate';
        this.isSaving = true;
        if (this.entity.id !== undefined) {
            this.subscribeToSaveResponse(this.getService().update(this.entity));
        }
        else {
            this.subscribeToSaveResponse(this.getService().create(this.entity));
        }
    };
    BaseEntityEditComponent.prototype.subscribeToSaveResponse = function (result) {
        var _this = this;
        result.subscribe(function (res) { return _this.onSaveSuccess(res.body); }, function (res) {
            _this.onSaveError();
            _this.onError(res.message);
        });
    };
    BaseEntityEditComponent.prototype.onSaveSuccess = function (result) {
        this.isSaving = false;
        this.previousState();
    };
    BaseEntityEditComponent.prototype.onSaveError = function () {
        this.isSaving = false;
        this.error = true;
        this.submitButton.disabled = true;
        this.progressBar.mode = 'determinate';
    };
    BaseEntityEditComponent.prototype.onError = function (errorMessage) {
        this.notification.openSnackMessage(errorMessage);
    };
    BaseEntityEditComponent.prototype.entityCompare = function (s1, s2) {
        return s1 && s2 ? s1.id == s2.id : s1 === s2;
    };
    tslib_1.__decorate([
        ViewChild(MatProgressBar, { static: true }),
        tslib_1.__metadata("design:type", MatProgressBar)
    ], BaseEntityEditComponent.prototype, "progressBar", void 0);
    tslib_1.__decorate([
        ViewChild(MatButton, { static: true }),
        tslib_1.__metadata("design:type", MatButton)
    ], BaseEntityEditComponent.prototype, "submitButton", void 0);
    return BaseEntityEditComponent;
}());
export { BaseEntityEditComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1lbnRpdHktZWRpdC1jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvYmFzZS1lbnRpdHktZWRpdC1jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUVBLE9BQU8sRUFBVSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQU05RDtJQU9JLGlDQUFzQixZQUFpQyxFQUNqQyxLQUFxQjtRQURyQixpQkFBWSxHQUFaLFlBQVksQ0FBcUI7UUFDakMsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFIM0MsVUFBSyxHQUFHLEtBQUssQ0FBQztJQUtkLENBQUM7SUFFRCwwQ0FBUSxHQUFSO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBQyxFQUFRO2dCQUFQLGtCQUFNO1lBQzlCLEtBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDakUsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssU0FBUyxFQUFFO1lBQzNCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ3JDO0lBQ0wsQ0FBQztJQUVELCtDQUFhLEdBQWI7UUFDSSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxzQ0FBSSxHQUFKLFVBQUssS0FBVztRQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxlQUFlLENBQUM7UUFDeEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxTQUFTLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7U0FDdkU7YUFBTTtZQUNILElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ3ZFO0lBQ0wsQ0FBQztJQUVPLHlEQUF1QixHQUEvQixVQUFnQyxNQUFxQztRQUFyRSxpQkFPQztRQU5HLE1BQU0sQ0FBQyxTQUFTLENBQ1osVUFBQyxHQUFzQixJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQTVCLENBQTRCLEVBQ3hELFVBQUMsR0FBc0I7WUFDbkIsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25CLEtBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVPLCtDQUFhLEdBQXJCLFVBQXNCLE1BQVc7UUFDN0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFTyw2Q0FBVyxHQUFuQjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7SUFDMUMsQ0FBQztJQUVTLHlDQUFPLEdBQWpCLFVBQWtCLFlBQW9CO1FBQ2xDLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELCtDQUFhLEdBQWIsVUFBYyxFQUFPLEVBQUUsRUFBTztRQUMxQixPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQztJQUNqRCxDQUFDO0lBL0QwQztRQUExQyxTQUFTLENBQUMsY0FBYyxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDOzBDQUFjLGNBQWM7Z0VBQUM7SUFDakM7UUFBckMsU0FBUyxDQUFDLFNBQVMsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQzswQ0FBZSxTQUFTO2lFQUFDO0lBbUVsRSw4QkFBQztDQUFBLEFBckVELElBcUVDO1NBckVxQix1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuaW1wb3J0IHsgSHR0cEVycm9yUmVzcG9uc2UsIEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgT25Jbml0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0QnV0dG9uLCBNYXRQcm9ncmVzc0JhciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IElCYXNlRW50aXR5IH0gZnJvbSAnLi4vc2hhcmVkL21vZGVsL2Jhc2UtZW50aXR5JztcclxuXHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQmFzZUVudGl0eUVkaXRDb21wb25lbnQ8VCBleHRlbmRzIElCYXNlRW50aXR5PiBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBAVmlld0NoaWxkKE1hdFByb2dyZXNzQmFyLCB7c3RhdGljOiB0cnVlfSkgcHJvZ3Jlc3NCYXI6IE1hdFByb2dyZXNzQmFyO1xyXG4gICAgQFZpZXdDaGlsZChNYXRCdXR0b24sIHtzdGF0aWM6IHRydWV9KSBzdWJtaXRCdXR0b246IE1hdEJ1dHRvbjtcclxuICAgIGVudGl0eTogVDtcclxuICAgIGlzU2F2aW5nOiBib29sZWFuO1xyXG4gICAgZXJyb3IgPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgbm90aWZpY2F0aW9uOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJvdGVjdGVkIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmlzU2F2aW5nID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5yb3V0ZS5kYXRhLnN1YnNjcmliZSgoe2VudGl0eX0pID0+IHtcclxuICAgICAgICAgICAgdGhpcy5lbnRpdHkgPSAhIWVudGl0eSAmJiBlbnRpdHkuYm9keSA/IGVudGl0eS5ib2R5IDogZW50aXR5O1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmICh0aGlzLmVudGl0eSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW50aXR5ID0gdGhpcy5jcmVhdGVFbnRpdHkoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJldmlvdXNTdGF0ZSgpIHtcclxuICAgICAgICB3aW5kb3cuaGlzdG9yeS5iYWNrKCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2F2ZSh2YWxpZD86IGFueSkge1xyXG4gICAgICAgIHRoaXMuc3VibWl0QnV0dG9uLmRpc2FibGVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnByb2dyZXNzQmFyLm1vZGUgPSAnaW5kZXRlcm1pbmF0ZSc7XHJcbiAgICAgICAgdGhpcy5pc1NhdmluZyA9IHRydWU7XHJcbiAgICAgICAgaWYgKHRoaXMuZW50aXR5LmlkICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5zdWJzY3JpYmVUb1NhdmVSZXNwb25zZSh0aGlzLmdldFNlcnZpY2UoKS51cGRhdGUodGhpcy5lbnRpdHkpKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnN1YnNjcmliZVRvU2F2ZVJlc3BvbnNlKHRoaXMuZ2V0U2VydmljZSgpLmNyZWF0ZSh0aGlzLmVudGl0eSkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHN1YnNjcmliZVRvU2F2ZVJlc3BvbnNlKHJlc3VsdDogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8YW55Pj4pIHtcclxuICAgICAgICByZXN1bHQuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAocmVzOiBIdHRwUmVzcG9uc2U8YW55PikgPT4gdGhpcy5vblNhdmVTdWNjZXNzKHJlcy5ib2R5KSxcclxuICAgICAgICAgICAgKHJlczogSHR0cEVycm9yUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMub25TYXZlRXJyb3IoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMub25FcnJvcihyZXMubWVzc2FnZSlcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvblNhdmVTdWNjZXNzKHJlc3VsdDogYW55KSB7XHJcbiAgICAgICAgdGhpcy5pc1NhdmluZyA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMucHJldmlvdXNTdGF0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25TYXZlRXJyb3IoKSB7XHJcbiAgICAgICAgdGhpcy5pc1NhdmluZyA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZXJyb3IgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuc3VibWl0QnV0dG9uLmRpc2FibGVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnByb2dyZXNzQmFyLm1vZGUgPSAnZGV0ZXJtaW5hdGUnO1xyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCBvbkVycm9yKGVycm9yTWVzc2FnZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb24ub3BlblNuYWNrTWVzc2FnZShlcnJvck1lc3NhZ2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGVudGl0eUNvbXBhcmUoczE6IGFueSwgczI6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBzMSAmJiBzMiA/IHMxLmlkID09IHMyLmlkIDogczEgPT09IHMyO1xyXG4gICAgfVxyXG5cclxuICAgIGFic3RyYWN0IGdldFNlcnZpY2UoKTogYW55O1xyXG5cclxuICAgIGFic3RyYWN0IGNyZWF0ZUVudGl0eSgpOiBUO1xyXG59XHJcbiJdfQ==