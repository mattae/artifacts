import { ModuleWithProviders } from '@angular/core';
import { ServerApiUrlConfig } from './app.constants';
import { LocaleConfig } from './shared/util/date-range-picker/date-range-picker.config';
export declare class LamisCoreModule {
    static forRoot(serverApiUrlConfig: ServerApiUrlConfig, config?: LocaleConfig): ModuleWithProviders;
}
