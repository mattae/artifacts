import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AccountService } from './account.service';
import { StateStorageService } from './state-storage.service';
export declare class UserRouteAccessService implements CanActivate {
    private router;
    private accountService;
    private stateStorageService;
    constructor(router: Router, accountService: AccountService, stateStorageService: StateStorageService);
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean>;
    checkLogin(authorities: string[], url: string): Promise<boolean>;
}
