import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from '../../services/login.service';
export declare class AuthExpiredInterceptor implements HttpInterceptor {
    private loginService;
    constructor(loginService: LoginService);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
